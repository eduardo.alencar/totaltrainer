//
//  PhysicalDobra.m
//  TotalTrainer
//
//  Created by Eduardo on 08/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "PhysicalDobra.h"

@implementation PhysicalDobra

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSError *erro = nil;
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarPhysicaldobra];
    }
    return self;
}

+(PhysicalDobra *)physicalDobraCompartilhado {
    static PhysicalDobra *physicalDobraCompartilhado = nil;
    if (!physicalDobraCompartilhado) physicalDobraCompartilhado = [[super allocWithZone:nil] init];
    return physicalDobraCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self physicalDobraCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_PhysicalDobra *)physicalDobra{
    [itens addObject:physicalDobra];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_PhysicalDobra *)physicalDobra {
    [contexto deleteObject:physicalDobra];
    [itens removeObject:physicalDobra];
    [self salvarMudancas];
}

-(void)carregarPhysicaldobra {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_PhysicalDobra"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"physicalplan_id" ascending:NO];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta PhysicalDobra" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(NSMutableArray *)carregarPhysicaldobra:(NSString *)plan_id {
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_PhysicalDobra"];
    [requisicao setEntity:entidade];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"plan_id CONTAINS %@ ",plan_id];
    [requisicao setPredicate:predicate];
    NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"physicalplan_id" ascending:YES];
    [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta PhysicalPerime" format:@"Motivo: %@", [erro localizedDescription]];
    NSMutableArray *listtotal =  [resultado copy];
    NSMutableArray *listaG = [[NSMutableArray alloc] init];
    for (int i = 0; [listtotal count] > i; i++) {
        
    }
    return listaG;
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarPhysicaldobra];
    return sucess;
}

-(S4M_PhysicalDobra *)criarPhysicaldobra:(NSString *)physicalplan_id setPanturrilha:(NSNumber *)panturrilha setPeitoral:(NSNumber *)peitoral setAbdomen:(NSNumber *)abdomen setQuadriceps:(NSNumber *)quadriceps setSubescapular:(NSNumber *)subescapular setSuprailiaca:(NSNumber *)suprailiaca setTriciptal:(NSNumber *)triciptal {
    S4M_PhysicalDobra *physical = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_PhysicalDobra" inManagedObjectContext:contexto];
    [physical setPhysicalplan_id:[NSNumber numberWithInteger: [physicalplan_id integerValue]]];
    [physical setPanturrilha:panturrilha];
    [physical setPeitoral:peitoral];
    [physical setAbdominal:abdomen];
    [physical setQuadriceps:quadriceps];
    [physical setSubescapular:subescapular];
    [physical setSuprailiaca:suprailiaca];
    [physical setTriciptal:triciptal];
    [itens addObject:physical];
    
    [self salvarMudancas];
    [self carregarPhysicaldobra];
    return physical;
}

-(S4M_PhysicalDobra *)upgradePhysicaldobra:(int)pos setPlan:(NSString *)physicalplan_id setPanturrilha:(NSNumber *)panturrilha setPeitoral:(NSNumber *)peitoral setAbdomen:(NSNumber *)abdomen setQuadriceps:(NSNumber *)quadriceps setSubescapular:(NSNumber *)subescapular setSuprailiaca:(NSNumber *)suprailiaca setTriciptal:(NSNumber *)triciptal {
    [self carregarPhysicaldobra];
    S4M_PhysicalDobra *physical = [itens objectAtIndex:pos];
    [physical setPhysicalplan_id:[NSNumber numberWithInteger: [physicalplan_id integerValue]]];
    [physical setPanturrilha:panturrilha];
    [physical setPeitoral:peitoral];
    [physical setAbdominal:abdomen];
    [physical setQuadriceps:quadriceps];
    [physical setSubescapular:subescapular];
    [physical setSuprailiaca:suprailiaca];
    [physical setTriciptal:triciptal];
    [self salvarMudancas];
    [self carregarPhysicaldobra];
    return physical;
}

-(void)excluirPhysicaldobra:(S4M_PhysicalDobra *)physicaldobra {
    [contexto deleteObject:physicaldobra];
    [itens removeObjectIdenticalTo:physicaldobra];
}

@end
