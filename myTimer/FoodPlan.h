//
//  FoodPlan.h
//  TotalTrainer
//
//  Created by Eduardo on 08/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Global.h"
#import "S4M_FoodPlan.h"

@interface FoodPlan : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(FoodPlan *)foodPlanCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(S4M_FoodPlan *)foodplan;
-(void)removerItem:(S4M_FoodPlan *)foodplan;
-(BOOL)salvarMudancas;
-(S4M_FoodPlan *)criarFoodPlan:(NSString *)food_id setType:(NSString *)type setQTD:(NSNumber *)qtd setDayWeek:(NSString *)dayWeek setHour:(NSString *)hour;
-(S4M_FoodPlan *)upgradeFoodPlan:(int)pos setFood:(NSString *)food_id setType:(NSString *)type setQTD:(NSNumber *)qtd setDayWeek:(NSString *)dayWeek setHour:(NSString *)hour;
-(void)excluirFoodPlan:(S4M_FoodPlan *)foodplan;
-(void)carregarFoodPlan;
-(NSMutableArray *)carregarFoodPlan:(NSString *)food_id;

@end
