//
//  FoodEditViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 11/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "FoodEditViewController.h"

@interface FoodEditViewController ()

@end

@implementation FoodEditViewController

@synthesize tproteina;
@synthesize tbarcode;
@synthesize tname;
@synthesize tcategory;
@synthesize ttype;
@synthesize tcarboidrato;
@synthesize tsodio;
@synthesize tcalorias;
@synthesize ltitle;
@synthesize lname;
@synthesize lcategory;
@synthesize ltype;
@synthesize lbarcode;
@synthesize lproteina;
@synthesize lcarboidrato;
@synthesize lsodio;
@synthesize lcalorias;
@synthesize bsave;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)savedFood:(id) sender {
    
}

@end
