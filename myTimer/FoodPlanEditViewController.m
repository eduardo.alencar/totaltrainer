//
//  FoodPlanEditViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 11/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "FoodPlanEditViewController.h"

@interface FoodPlanEditViewController ()

@end

@implementation FoodPlanEditViewController

@synthesize tname;
@synthesize tday_week;
@synthesize thour;
@synthesize tqtd;
@synthesize ttype;
@synthesize ltitle;
@synthesize lname;
@synthesize lday_week;
@synthesize lhour;
@synthesize lqtd;
@synthesize ltype;
@synthesize bsave;
@synthesize bfood;
@synthesize bschedule;
@synthesize lalarm;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)savedFoodPlan:(id) sender {
    
}

-(IBAction)editFoodPlan:(id) sender {
    
}


@end
