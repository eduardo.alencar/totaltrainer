//
//  S4M_HeartRateMonitor.m
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "S4M_HeartRateMonitor.h"


@implementation S4M_HeartRateMonitor

@dynamic bpm;
@dynamic data;
@dynamic id;

@end
