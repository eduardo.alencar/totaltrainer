//
//  LocationController.h
//  myTimer
//
//  Created by Eduardo on 22/09/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationControllerDelegate;

@interface LocationController : NSObject <CLLocationManagerDelegate> {
	CLLocationManager *locMgr;
	id delegate;
}

@property (nonatomic, retain) CLLocationManager *locMgr;
@property (nonatomic, retain) id delegate;


@end
