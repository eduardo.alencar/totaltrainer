//
//  WorkoutTableViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 09/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrainerTableViewController.h"

@interface WorkoutTableViewController : UIViewController  <UITableViewDelegate, UITableViewDataSource> {
    UITableViewCell *tableViewCelula;
    UITableView *tableTreino;
    UILabel *ldata, *lnome, *lobj, *ltitle;
    UIAlertView *alert;
    NSMutableArray *lista;
    TrainerTableViewController *trainerController;
    IBOutlet myTimerViewController *myTimerController;
}

@property(nonatomic, retain) IBOutlet UITableViewCell *tableViewCelula;
@property(nonatomic, retain) IBOutlet UITableView *tableTreino;
@property(nonatomic, retain) IBOutlet UILabel *ldata, *lnome, *lobj, *ltitle;
@property(nonatomic, retain) TrainerTableViewController *trainerController;

-(void)getInfoWorkout;
-(void)reloadTableView;
-(IBAction)sendBack:(id) sender;
-(IBAction)sendAddWorkout:(id) sender;


@end
