//
//  ExercisePlan.h
//  TotalTrainer
//
//  Created by Eduardo on 04/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S4M_ExercisePlan.h"
#import "Global.h"

@interface ExercisePlan  : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(ExercisePlan *)exercisePlanCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(S4M_ExercisePlan *)exerciseplan;
-(void)removerItem:(S4M_ExercisePlan *)exerciseplan;
-(BOOL)salvarMudancas;
-(S4M_ExercisePlan *)criarExercisePlan:(NSString *)name setDateBegin:(NSDate *)datebegin setDateEnd:(NSDate *)dateend setHour:(NSString *)hour setObjective:(NSString *)objective;
-(S4M_ExercisePlan *)upgradeExercisePlan:(int) pos setName:(NSString *)name setDateBegin:(NSDate *)datebegin setDateEnd:(NSDate *)dateend setHour:(NSString *)hour setObjective:(NSString *)objective;

-(void)excluirExercicioPlan:(S4M_ExercisePlan *)exerc;
-(void)carregarExercisePlan;

@end
