//
//  Version.h
//  TotalTrainer
//
//  Created by Eduardo on 31/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S4M_Version.h"
#import "Global.h"

@interface Version : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
    NSString *VERSION;
}

+(Version *)versionCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(NSString *)version;
-(BOOL)salvarMudancas;
-(S4M_Version *)criarVersion;
-(void)carregarVersion;
-(void)migrarVersion;


@end
