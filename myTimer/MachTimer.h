//
//  MachTimer.h
//  TotalTrainer
//
//  Created by Eduardo on 09/11/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <mach/mach_time.h>

@interface MachTimer : NSObject{
    uint64_t timeZero;
}

+(id) timer;
-(void) start;
-(uint64_t) elapsed;
-(float) elapsedSeconds;

@end
