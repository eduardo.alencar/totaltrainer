//
//  ProfileViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 12/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "ProfileViewController.h"
#import "Global.h"
#import "Perfil.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize title;
@synthesize tname;
@synthesize tbirthday;
@synthesize ltitle;
@synthesize lbirthday;
@synthesize lfemale;
@synthesize lgender;
@synthesize lmale;
@synthesize lname;
@synthesize lphoto;
@synthesize bsave;
@synthesize bgender;

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    profile = [[Perfil alloc] init];
    [self reloadInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)sendBack:(id) sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)reloadInfo {
    linfo = [[Perfil perfilCompartilhado] itens];
    NSLog(@"INFO -> PROFILE %@", linfo  );
    if ([linfo count] > 0) {
        user = [linfo objectAtIndex:0];
        [tname setText:[user name]];
        NSLog(@"INFO -> PROFILE %@", user);
    }
}


@end
