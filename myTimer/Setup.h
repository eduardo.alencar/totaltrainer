//
//  Setup.h
//  TotalTrainer
//
//  Created by Eduardo on 31/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S4M_Setup.h"
#import "Global.h"

@interface Setup : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(Setup *)setupCompartilhado;
-(NSMutableArray *)itens;
-(void)adicionarItem:(NSString *)setup;
-(BOOL)salvarMudancas;
-(S4M_Setup *)criarSetup:(NSString *)pass setLogin:(NSString *)login setFacebook:(NSNumber *)facebook setGPS:(NSNumber *) gps setTotalHour:(NSNumber *) total_hour setTotalMin:(NSNumber *)total_min setIntervalMin:(NSNumber *)interval_min setIntervalSec:(NSNumber *) interval_sec setUnit:(NSString *) unit setGrupo:(NSString *) grupo setSound:(NSNumber *) sound setSpeak:(NSNumber *) speak;
-(S4M_Setup *)upgradeSetup:(NSString *)pass setLogin:(NSString *)login setFacebook:(NSNumber *)facebook setGPS:(NSNumber *) gps setTotalHour:(NSNumber *) total_hour setTotalMin:(NSNumber *)total_min setIntervalMin:(NSNumber *)interval_min setIntervalSec:(NSNumber *) interval_sec setUnit:(NSString *) unit setGrupo:(NSString *) grupo setSound:(NSNumber *) sound setSpeak:(NSNumber *) speak;
-(void)carregarSetup;
-(void)migrarSetup :(NSMutableArray *)Info;
-(NSMutableArray *)getIntervaloGeral;

@end
