//
//  LoginViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 29/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize alert;
@synthesize ltitile_login, linfo1, linfo2;
@synthesize timerController;
@synthesize bskip;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIGraphicsBeginImageContext(self.view.frame.size);
    if (IS_IPHONE_5) [[UIImage imageNamed:@"fundo-login-02.png"] drawInRect:self.view.bounds];
    else [[UIImage imageNamed:@"fundo-login-01.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [ltitile_login setFont:[UIFont fontWithName:@"DINEngschriftStd" size:26]];
    [linfo1 setFont:[UIFont fontWithName:@"Exo-Medium" size:18]];
    [linfo1 setText:NSLocalizedString(@"login_info1", @"")];
    [linfo2 setFont:[UIFont fontWithName:@"Exo-Medium" size:18]];
    [linfo2 setText:NSLocalizedString(@"login_info2", @"")];
    [bskip setFont:[UIFont fontWithName:@"Exo-Medium" size:22]];
    [bskip setTitle:NSLocalizedString(@"login_skip", @"") forState:UIControlStateNormal];
    user_id = @"";
    SOUND = YES;
    SPEAK = NO;

    [self configButtonFB];
}

-(void)configButtonFB {
    FBLoginView *loginview = [[FBLoginView alloc] init];
    if (IS_IPHONE_5) loginview.frame = CGRectMake(70, 190, 300, 74);
    else loginview.frame = CGRectMake(70, 190, 176, 74);
    for (id obj in loginview.subviews){
        if ([obj isKindOfClass:[UIButton class]]){
            UIButton *loginButton =  obj;
            UIImage *loginImage = [UIImage imageNamed:@"bot-face-off-01.png"];
            
            [loginButton setBackgroundImage:loginImage forState:UIControlStateNormal];
            [loginButton setBackgroundImage:nil forState:UIControlStateSelected];
            [loginButton setBackgroundImage:loginImage forState:UIControlStateHighlighted];
            [loginButton sizeToFit];
        }
        if ([obj isKindOfClass:[UILabel class]]){
            UILabel *loginLabel =  obj;
            loginLabel.text = @"";
            loginLabel.textAlignment = NSTextAlignmentCenter;
            loginLabel.frame = CGRectMake(0, 0, 0, 0);
        }
    }
    loginview.delegate = self;
    [self.view addSubview:loginview];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)LoginRunKeeperClick:(UIButton *)sender{}

-(IBAction)SkipClick:(UIButton *)sender {
    alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Loading", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    fbID = @"";
    MainViewController *tvcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"main_view"];
    self.timerController = tvcontroller;
    [self.view addSubview:tvcontroller.view];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    fbID = ![user.username isEqual:nil]? [NSString stringWithFormat:@"%@",user.username] : @"";
    if (DEBUG_LOG) NSLog(@"User FB -> %@",fbID);
    if (DEBUG_LOG) NSLog(@"User -> %@", [NSString stringWithFormat:@"%@ %@",user.first_name, user.last_name ]);
    if (DEBUG_LOG) NSLog(@"Aniversario  -> %@", user.birthday);
    NSString *userPhoto = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",user.username];
    if (DEBUG_LOG) NSLog(@"User foto -> %@",userPhoto);
    if (DEBUG_LOG) NSLog(@"User Sexo -> %@",[user objectForKey:@"gender"]);
    if (DEBUG_LOG) NSLog(@"User cidade -> %@",user.location[@"name"]);
    if (DEBUG_LOG) NSLog(@"User email -> %@",[user objectForKey:@"email"]);
    
    NSString *dateStr = user.birthday;
    dateStr = [NSString stringWithFormat:@"%@%@%@",[dateStr substringWithRange:NSMakeRange(6,4)],[dateStr substringWithRange:NSMakeRange(3,2)],[dateStr substringWithRange:NSMakeRange(0,2)]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSDate *dateBirthday = [dateFormat dateFromString:dateStr];
    [[Perfil perfilCompartilhado] criarPerfil:fbID setUsername:user.username setName:[NSString stringWithFormat:@"%@ %@",user.first_name, user.last_name ] setGender:[user objectForKey:@"gender"] setBirthday:dateStr setCity:user.location[@"name"] setEmail:[user objectForKey:@"email"] setPicture:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",user.username]];
    if ([fbID length] > 2) {
        // MIGRAR PARA COREDATA
        //[banco atualizarFB:fbID];
        //myTimerViewController *tvcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"timer_view"];
        
        //infoPessoal = [[Perfil alloc] init];
        //[infoPessoal upgradePerfil:fbID setUsername:fbID setName:[NSString stringWithFormat:@"%@ %@",user.first_name, user.last_name ] setGender:[user objectForKey:@"gender"] setBirthday:dateBirthday setCity:user.location[@"name"] setEmail:[user objectForKey:@"email"] setPicture:userPhoto];
        NSLog(@"Atualizando dados do FB ...");

        MainViewController *tvcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"main_view"];
        self.timerController = tvcontroller;
        [self.view addSubview:tvcontroller.view];
    }
}

-(IBAction)S4MClick:(UIButton *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.solution4mac.com.br"]];
}

@end
