//
//  S4M_Perfil.m
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "S4M_Perfil.h"


@implementation S4M_Perfil

@dynamic birthday;
@dynamic id;
@dynamic name;
@dynamic gender;
@dynamic picture;
@dynamic city;
@dynamic fbid;
@dynamic email;

@end
