//
//  Global.m
//  TotalTrainer
//
//  Created by Eduardo on 29/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "Global.h"

@implementation Global

NSString *user, *fbID, *user_id, *treino_id, *VERSION;
Boolean fbB, SOUND, SPEAK, DEBUG_LOG;
NSMutableArray *aTrainer;
int contG, maxG, treinogeral;
S4M_Exercise *exerc_detalhe;
NSURL *ubiq;
UITextField *textAlertGroup, *textAlertExercise, *textAlertSerie, *textAlertRepeat, *textAlertweight, *tbfAlertweight, *tbfAlertCoxa, *tbfAlertPeito, *tbfAlertAbdomen, *tbfAlertIdade, *tbfAlertPeso;
UIView *viewFront;

@end
