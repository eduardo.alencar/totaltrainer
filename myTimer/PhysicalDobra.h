//
//  PhysicalDobra.h
//  TotalTrainer
//
//  Created by Eduardo on 08/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Global.h"
#import "S4M_PhysicalDobra.h"

@interface PhysicalDobra : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(PhysicalDobra *)physicalDobraCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(S4M_PhysicalDobra *)physicalDobra;
-(void)removerItem:(S4M_PhysicalDobra *)physicalDobra;
-(BOOL)salvarMudancas;
-(S4M_PhysicalDobra *)criarPhysicaldobra:(NSString *)physicalplan_id setPanturrilha:(NSNumber *)panturrilha setPeitoral:(NSNumber *)peitoral setAbdomen:(NSNumber *)abdomen setQuadriceps:(NSNumber *)quadriceps setSubescapular:(NSNumber *)subescapular setSuprailiaca:(NSNumber *)suprailiaca setTriciptal:(NSNumber *)triciptal;
-(S4M_PhysicalDobra *)upgradePhysicaldobra:(int)pos setPlan:(NSString *)physicalplan_id setPanturrilha:(NSNumber *)panturrilha setPeitoral:(NSNumber *)peitoral setAbdomen:(NSNumber *)abdomen setQuadriceps:(NSNumber *)quadriceps setSubescapular:(NSNumber *)subescapular setSuprailiaca:(NSNumber *)suprailiaca setTriciptal:(NSNumber *)triciptal;
-(void)excluirPhysicaldobra:(S4M_PhysicalDobra *)physicaldobra;
-(void)carregarPhysicaldobra;
-(NSMutableArray *)carregarPhysicaldobra:(NSString *)plan_id;

@end
