//
//  ProfileViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 12/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Perfil.h"

@interface ProfileViewController : UIViewController<UITextFieldDelegate>  {
    UIImageView *lphoto;
    UIButton *bgender, *bsave;
    UILabel *ltitle, *lname, *lgender, *lfemale, *lmale, *lbirthday;
    UITextField *tname, *tbirthday;
    Perfil *profile;
    NSMutableArray *linfo;
    S4M_Perfil *user;
}

@property(nonatomic, retain) IBOutlet UIImageView *lphoto;
@property(nonatomic, retain) IBOutlet UIButton *bgender, *bsave;
@property(nonatomic, retain) IBOutlet UILabel *ltitle, *lname, *lgender, *lfemale, *lmale, *lbirthday;
@property(nonatomic, retain) IBOutlet UITextField *tname, *tbirthday;

-(IBAction)sendBack:(id) sender;
-(void)reloadInfo;

@end
