//
//  Perfil.m
//  TotalTrainer
//
//  Created by Eduardo on 04/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "Perfil.h"

@implementation Perfil
-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSLog(@"[Perfil] DIRETORIO PARA iCLOUD -> %@", urlArmazenamento);
        NSError *erro = nil;
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:nil error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarPerfil];
    }
    return self;
}

+(Perfil *)perfilCompartilhado {
    static Perfil *perfilCompartilhado = nil;
    if (!perfilCompartilhado) perfilCompartilhado = [[super allocWithZone:nil] init];
    return perfilCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self perfilCompartilhado];
}

-(NSMutableArray *)itens {
    return itens;
}

-(void)adicionarItem:(NSString *)perfil{
    [itens addObject:perfil];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)carregarPerfil {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Perfil"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta Perfil" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarPerfil];
    return sucess;
}

-(S4M_Perfil *)criarPerfil:(NSString *)fbID setUsername:(NSString *)username setName:(NSString *)name setGender:(NSString *)gender setBirthday:(NSDate *)day setCity:(NSString *)city setEmail:(NSString *)email setPicture:(NSString *)photo {
    S4M_Perfil *perfil = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Perfil" inManagedObjectContext:contexto];
    [itens removeAllObjects];
    [perfil setFbid:fbID];
    [perfil setName:username];
    [perfil setName:name];
    [perfil setGender:gender];
    [perfil setBirthday:day];
    [perfil setCity:city];
    [perfil setEmail:email];
    [perfil setPicture:photo];
    
    [itens addObject:perfil];
    [self salvarMudancas];
    [self carregarPerfil];
    return perfil;
}

-(S4M_Perfil *)upgradePerfil:(NSString *)fbID setUsername:(NSString *)username setName:(NSString *)name setGender:(NSString *)gender setBirthday:(NSDate *)day setCity:(NSString *)city setEmail:(NSString *)email setPicture:(NSString *)photo{
    [self carregarPerfil];
    S4M_Perfil *perfil;
    if ([itens count] > 0) {
        NSLog(@"Atualizando perfil ... %@ - %i", [itens objectAtIndex:0], [itens count]);
        perfil = [itens objectAtIndex:0];
        NSLog(@"PASSO 1");
        //[perfil setFbid:fbID];
        NSLog(@"PASSO 2");
        [perfil setName:username];
        NSLog(@"PASSO 3");
        [perfil setName:name];
        NSLog(@"PASSO 4");
        [perfil setGender:gender];
        NSLog(@"PASSO 5");
        [perfil setBirthday:day];
        NSLog(@"PASSO 6");
        [perfil setCity:city];
        NSLog(@"PASSO 7");
        [perfil setEmail:email];
        NSLog(@"PASSO 8");
        [perfil setPicture:photo];
        NSLog(@"PASSO 9");
    } else {
        perfil = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Perfil" inManagedObjectContext:contexto];
        NSLog(@"Criando perfil ...");
        [itens removeAllObjects];
        [perfil setFbid:fbID];
        [perfil setName:username];
        [perfil setName:name];
        [perfil setGender:gender];
        [perfil setBirthday:day];
        [perfil setCity:city];
        [perfil setEmail:email];
        [perfil setPicture:photo];
        [itens addObject:perfil];
    }
    [self salvarMudancas];
    [self carregarPerfil];
    return perfil;
}

//MIGRAÇÃO
-(void)migrarPerfil:(NSMutableArray *)Info {
    NSLog(@"dados para migraçao recebidos [%i] -> %@", [Info count],Info);
    if  ([Info count] == 7) {
        S4M_Perfil *sp = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Perfil" inManagedObjectContext:contexto];
        [sp setId:[NSNumber numberWithInt:1]];
        [sp setFbid:[Info objectAtIndex:0]];
        [sp setName:[Info objectAtIndex:1]];
        [sp setGender:[Info objectAtIndex:2]];
        [sp setBirthday:[Info objectAtIndex:3]];
        [sp setCity:[Info objectAtIndex:4]];
        [sp setEmail:[Info objectAtIndex:5]];
        [sp setPicture:[Info objectAtIndex:6]];
        [contexto deletedObjects];
        [itens removeAllObjects];
        [self salvarMudancas];
        [itens addObject:sp];
        [self salvarMudancas];
        [self carregarPerfil];
    }
}

@end
