//
//  FoodSchedule.h
//  TotalTrainer
//
//  Created by Eduardo on 06/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S4M_FoodSchedule.h"
#import "Global.h"

@interface FoodSchedule : NSObject{
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(FoodSchedule *)foodScheduleCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(S4M_FoodSchedule *)foodschedule;
-(void)removerItem:(S4M_FoodSchedule *)foodschedule;
-(BOOL)salvarMudancas;
-(S4M_FoodSchedule *)criarFoodschedule:(NSString *)plan_id setDateBegin:(NSString *)datebegin setDateEnd:(NSString *)dateend;
-(S4M_FoodSchedule *)upgradeFoodschedule:(int)pos setPlan:(NSString *)plan_id setDateBegin:(NSString *)datebegin setDateEnd:(NSString *)dateend;
-(void)excluirFoodSchedule:(S4M_FoodSchedule *)foodschedule;
-(void)carregarFoodschedule;
-(NSMutableArray *)carregarFoodschedule:(NSString *)plan_id;
@end
