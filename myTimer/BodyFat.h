//
//  BodyFat.h
//  TotalTrainer
//
//  Created by Eduardo on 01/07/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BodyFat : NSObject {
    double siri, bf, densidade;
    
}

-(double)IMCUSP2012:(double)kg setBF:(double)bf setH:(int)cm;
-(double)JacksonPollock3Dobras:(double)Sum setAge:(double)Age setGender:(NSString *)gender;

@end
