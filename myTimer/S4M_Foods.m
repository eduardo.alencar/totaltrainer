//
//  S4M_Foods.m
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "S4M_Foods.h"


@implementation S4M_Foods

@dynamic calories;
@dynamic carbohydrates;
@dynamic code;
@dynamic id;
@dynamic name;
@dynamic proteins;
@dynamic sodio;
@dynamic category;
@dynamic type;

@end
