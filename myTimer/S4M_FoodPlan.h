//
//  S4M_FoodPlan.h
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_FoodPlan : NSManagedObject

@property (nonatomic, retain) NSNumber * food_id;
@property (nonatomic, retain) NSString * hour;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * qtd;
@property (nonatomic, retain) NSString * tipo;
@property (nonatomic, retain) NSString * day_week;

@end
