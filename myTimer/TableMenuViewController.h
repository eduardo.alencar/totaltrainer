//
//  TableMenuViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 30/01/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BodyFatViewController.h"
#import "Global.h"

@protocol TableMenuViewControllerDelegate <NSObject>
@end

@interface TableMenuViewController : UITableViewController {
    
}

@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, weak)   IBOutlet UITableViewCell *cellMain;
@property (nonatomic, strong) NSMutableArray *arrayOfAnimals;
@property (nonatomic, assign) id<TableMenuViewControllerDelegate> delegate;
@property (nonatomic, strong) UIView *viewMain;

@end
