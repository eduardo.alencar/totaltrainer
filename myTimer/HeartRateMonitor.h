//
//  HeartRateMonitor.h
//  TotalTrainer
//
//  Created by Eduardo on 06/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Global.h"
#import "S4M_HeartRateMonitor.h"

@interface HeartRateMonitor : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(HeartRateMonitor *)heartratemonitorCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(S4M_HeartRateMonitor *)heart;
-(void)removerItem:(S4M_HeartRateMonitor *)heart;
-(BOOL)salvarMudancas;
-(S4M_HeartRateMonitor *)criarHeartRateMonitor:(NSString *)data setBPM:(NSString *)bpm;
-(S4M_HeartRateMonitor *)upgradeExercise:(int)pos setData:(NSString *)data setBPM:(NSString *)bpm;

-(void)excluirHeartRateMonitor:(S4M_HeartRateMonitor *)heart;
-(void)carregarHeartRateMonitor;
-(NSMutableArray *)carregarHeartRateMonitor:(NSString *)day;

@end
