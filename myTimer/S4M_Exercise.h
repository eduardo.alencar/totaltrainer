//
//  S4M_Exercise.h
//  TotalTrainer
//
//  Created by Eduardo on 29/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_Exercise : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * plan_id;
@property (nonatomic, retain) NSString * conjugado;
@property (nonatomic, retain) NSString * grupo;
@property (nonatomic, retain) NSString * exercicio;
@property (nonatomic, retain) NSString * repeticao;
@property (nonatomic, retain) NSString * carga;
@property (nonatomic, retain) NSString * pos;
@property (nonatomic, retain) NSString * intervalo;

@end
