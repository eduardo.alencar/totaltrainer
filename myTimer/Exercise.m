//
//  Exercise.m
//  TotalTrainer
//
//  Created by Eduardo on 31/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "Exercise.h"

@implementation Exercise

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSError *erro = nil;
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
 
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarExercise];
    }
    return self;
}

+(Exercise *)exerciseCompartilhado {
    static Exercise *exerciseCompartilhado = nil;
    if (!exerciseCompartilhado) exerciseCompartilhado = [[super allocWithZone:nil] init];
        return exerciseCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self exerciseCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_Exercise *)exercise{
    [itens addObject:exercise];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_Exercise *)exercise {
    [contexto deleteObject:exercise];
    [itens removeObject:exercise];
    [self salvarMudancas];
}

-(void)carregarExercise {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Exercise"];
        [requisicao setEntity:entidade];
        
        //NSPredicate *predicate = [NSPredicate predicateWithFormat: @"ORDER BY grupo"];
        //[requisicao setPredicate:predicate];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"grupo" ascending:YES];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta Exercise" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(NSMutableArray *)carregarExercise:(NSString *)day {
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Exercise"];
    [requisicao setEntity:entidade];
    NSLog(@"DAY -> %@", day);
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"grupo CONTAINS %@ ",day];
    [requisicao setPredicate:predicate];
    NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"grupo" ascending:YES];
    [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta Exercise" format:@"Motivo: %@", [erro localizedDescription]];
    //NSMutableArray *listtotal =  [[NSMutableArray alloc] initWithArray:resultado];
    NSMutableArray *listtotal =  [resultado copy];
    NSMutableArray *listaG = [[NSMutableArray alloc] init];
    for (int i = 0; [listtotal count] > i; i++) {
        NSArray *arep = [[[listtotal objectAtIndex:i] repeticao] componentsSeparatedByString:@"/"];
        NSArray *apeso = [[[listtotal objectAtIndex:i] carga] componentsSeparatedByString:@"/"];
        for (int x = 0; x < [arep count]; x++) {
            /*
            NSMutableArray *listtmp = [listtotal copy];
            [[listtmp objectAtIndex:i] setCarga:([apeso count] > 1 ? [apeso objectAtIndex:x] : [[listtotal objectAtIndex:i] carga])];
            [[listtmp objectAtIndex:i] setRepeticao:[arep objectAtIndex:x]];
            [listaG addObject:[listtmp objectAtIndex:i]];
             */
            Trainer *treino = [[Trainer alloc] init];
            treino.exercicio = [[listtotal objectAtIndex:i] exercicio];
            treino.carga = ([apeso count] > 1 ? [apeso objectAtIndex:x] : [[listtotal objectAtIndex:i] carga]);
            treino.repeticao = [arep objectAtIndex:x];
            treino.grupo = [[listtotal objectAtIndex:i] grupo];
            [listaG addObject:treino];
        }
    }
    return listaG;
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
        itens = nil;
        [self carregarExercise];
    return sucess;
}

-(int)MaxTrainer {
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Exercise"];
    [requisicao setEntity:entidade];
    NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"grupo" ascending:YES];
    [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta Exercise" format:@"Motivo: %@", [erro localizedDescription]];
    if ([resultado count] > 0) {
        NSString *tmp;
        int MAX = 9;
        tmp = [[resultado objectAtIndex:[resultado count]-1] grupo];
        tmp = [[[[[[[[[[tmp stringByReplacingOccurrencesOfString:@"1" withString:@""] stringByReplacingOccurrencesOfString:@"2" withString:@""] stringByReplacingOccurrencesOfString:@"3" withString:@""] stringByReplacingOccurrencesOfString:@"4" withString:@""] stringByReplacingOccurrencesOfString:@"5" withString:@""] stringByReplacingOccurrencesOfString:@"6" withString:@""] stringByReplacingOccurrencesOfString:@"7" withString:@""] stringByReplacingOccurrencesOfString:@"8" withString:@""] stringByReplacingOccurrencesOfString:@"9" withString:@""] stringByReplacingOccurrencesOfString:@"0" withString:@""];
        NSArray *aalf = [NSArray arrayWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",nil];
        MAX = [aalf indexOfObject:tmp];
        NSLog(@"MAX ENCONTRANDO -> %i", MAX);
        return MAX;
    } else return 0;
}

-(S4M_Exercise *)criarExercise:(NSString *)grupo setExercise:(NSString *)exercicio setRepeat:(NSString *)repeticao setWeight:(NSString *)carga{
    S4M_Exercise *exercise = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Exercise" inManagedObjectContext:contexto];
    [exercise setGrupo:grupo];
    [exercise setExercicio:exercicio];
    [exercise setRepeticao:repeticao];
    [exercise setCarga:carga];
    [itens addObject:exercise];
    
    [self salvarMudancas];
    [self carregarExercise];
    return exercise;
}

-(S4M_Exercise *)upgradeExercise:(int) pos setGroup:(NSString *)grupo setExercise:(NSString *)exercicio setRepeat:(NSString *)repeticao setWeight:(NSString *)carga {
    [self carregarExercise];
    S4M_Exercise *exercise = [itens objectAtIndex:pos];
    [exercise setGrupo:grupo];
    [exercise setExercicio:exercicio];
    [exercise setRepeticao:repeticao];
    [exercise setCarga:carga];
    
    [self salvarMudancas];
    [self carregarExercise];
    return exercise;
}

-(void)excluirExercicio:(S4M_Exercise *)exerc {
    [contexto deleteObject:exerc];
    [itens removeObjectIdenticalTo:exerc];
}

//MIGRAÇÃO
-(void)migrarExercise :(NSMutableArray *)Info {
    if ([Info count] > 0) {
        Trainer *list;
        S4M_Exercise *ex;
        [contexto deletedObjects];
        [itens removeAllObjects];
        [self salvarMudancas];
        [self carregarExercise];
        for (int i = 0; i < [Info count]; i++){
            list = [Info objectAtIndex:i];
            if (list != nil) {
                ex = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Exercise" inManagedObjectContext:contexto];
                [ex setGrupo:[list grupo]];
                [ex setExercicio:[list exercicio]];
                [ex setRepeticao:[list repeticao]];
                [ex setCarga:[list carga]];
                [itens addObject:ex];
            }
        }
        [self salvarMudancas];
        [self carregarExercise];
    }
}

@end
