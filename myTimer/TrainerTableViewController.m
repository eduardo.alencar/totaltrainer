//
//  TrainerTableViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 29/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "TrainerTableViewController.h"

@interface TrainerTableViewController ()

@end

@implementation TrainerTableViewController

@synthesize tableTreino;
@synthesize tableViewCelula;
@synthesize lpos, lexerc, llbs, lrepeat, ltitle;
@synthesize editController;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Loading", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [ltitle setFont:[UIFont fontWithName:@"DINEngschriftStd" size:26]];
    [ltitle setText:NSLocalizedString(@"serie_title",@"")];
    [alert show];

    UIGraphicsBeginImageContext(self.view.frame.size);
    if (IS_IPHONE_5) [[UIImage imageNamed:@"fundo-massa-corporal-02.png"] drawInRect:self.view.bounds];
    else [[UIImage imageNamed:@"fundo-massa-corporal-01.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];

    exerc_detalhe = nil;
    treinogeral = -1;
    [self getInfoTreino];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
-(void)PopularListaTreino{
    treino = [[Trainer alloc]init];
    if (DEBUG_LOG) NSLog(@"POPULANDO TABELA dE TREINOS");
    lista = [[NSMutableArray alloc] init];
    for (int i = 0; i < [aTrainer count]; i++) {
        treino = [aTrainer objectAtIndex:i];
        NSDictionary *linha = [[NSDictionary alloc] initWithObjectsAndKeys:treino.grupo, @"pos", treino.exercicio, @"exercicio", treino.repeticao, @"repeticao", treino.peso, @"carga", nil];
        [lista addObject:linha];
        //NSLog(@"lista de banco -> [%@]", linha);
    }
    //NSLog(@"1 - Iniciando reload \n\n");
    [[self tableTreino] reloadData];
    //NSLog(@"1 - Finalizando reload \n\n");
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}
*/

#pragma mark Table View Data Source metodos
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (DEBUG_LOG) NSLog(@"TAMANHO DA TABELA DE PROJETOS -> [%i]", [lista count]);
    return [lista count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *celula = [self.tableTreino dequeueReusableCellWithIdentifier:@"trainer_cell"];
    if (DEBUG_LOG) NSLog(@"Entrou NO RELOAD -> %@ ", celula);
    
    /*
    NSUInteger linha = [indexPath row];
    NSDictionary *dadosLinha = [lista objectAtIndex:linha];
    UILabel *labelPos = (UILabel *)[celula.contentView viewWithTag:1001];
    labelPos.text = [dadosLinha objectForKey:@"pos"];
    labelPos.font = [UIFont fontWithName:@"Exo-ExtraBold" size:33];
    UILabel *labelExerc = (UILabel *)[celula.contentView viewWithTag:1002];
    labelExerc.text = [[dadosLinha objectForKey:@"exercicio"] uppercaseString];
    labelExerc.font = [UIFont fontWithName:@"Exo-Bold" size:18];
    labelExerc.numberOfLines = 3;
    UILabel *labelRepet = (UILabel *)[celula.contentView viewWithTag:1003];
    labelRepet.text = [dadosLinha objectForKey:@"repeticao"];
    labelRepet.font = [UIFont fontWithName:@"Exo-Medium" size:15];
    UILabel *labelCarga = (UILabel *)[celula.contentView viewWithTag:1004];
    labelCarga.text = [dadosLinha objectForKey:@"carga"];
    labelCarga.font = [UIFont fontWithName:@"Exo-Medium" size:15];
    celula.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    */
   
    S4M_Exercise *exerc = [[[Exercise exerciseCompartilhado] itens] objectAtIndex:[indexPath row]];
    UILabel *labelPos = (UILabel *)[celula.contentView viewWithTag:1001];
    labelPos.text = [exerc grupo];
    labelPos.font = [UIFont fontWithName:@"Exo-ExtraBold" size:33];
    UILabel *labelExerc = (UILabel *)[celula.contentView viewWithTag:1002];
    labelExerc.text = [[exerc exercicio] uppercaseString];
    labelExerc.font = [UIFont fontWithName:@"Exo-Bold" size:18];
    labelExerc.numberOfLines = 3;
    UILabel *labelRepet = (UILabel *)[celula.contentView viewWithTag:1003];
    labelRepet.text = [exerc repeticao];
    labelRepet.font = [UIFont fontWithName:@"Exo-Medium" size:15];
    UILabel *labelCarga = (UILabel *)[celula.contentView viewWithTag:1004];
    labelCarga.text = [exerc carga];
    labelCarga.font = [UIFont fontWithName:@"Exo-Medium" size:15];
    celula.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    //gesture
    UISwipeGestureRecognizer* sgr = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(cellSwiped:)];
    [sgr setDirection:UISwipeGestureRecognizerDirectionRight];
    [celula addGestureRecognizer:sgr];
    
    UIImageView *av = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 277, 58)];
    av.backgroundColor = [UIColor clearColor];
    av.opaque = NO;
    NSString *Day = [[[[[[[[[[[exerc grupo] stringByReplacingOccurrencesOfString:@"1" withString:@""] stringByReplacingOccurrencesOfString:@"2" withString:@""] stringByReplacingOccurrencesOfString:@"3" withString:@""] stringByReplacingOccurrencesOfString:@"4" withString:@""] stringByReplacingOccurrencesOfString:@"5" withString:@""] stringByReplacingOccurrencesOfString:@"6" withString:@""] stringByReplacingOccurrencesOfString:@"7" withString:@""] stringByReplacingOccurrencesOfString:@"8" withString:@""] stringByReplacingOccurrencesOfString:@"9" withString:@""] stringByReplacingOccurrencesOfString:@"0" withString:@""];
    
    if ([Day isEqualToString:@"A"]|| [Day isEqualToString:@"E"] || [Day isEqualToString:@"I"]) Day = @"fundo-serie-a-01.png";
    else if ([Day isEqualToString:@"B"] || [Day isEqualToString:@"F"] || [Day isEqualToString:@"J"]) Day = @"fundo-serie-b-01.png";
    else if ([Day isEqualToString:@"C"] || [Day isEqualToString:@"G"]) Day = @"fundo-serie-c-01.png";
    else Day = @"fundo-serie-d-01.png";

    av.image = [UIImage imageNamed:Day];
    celula.backgroundView = av;
    
    return celula;
}

-(void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if (DEBUG_LOG) NSLog (@"Entrou na willBeginEditingRow ..." );
    /*
    NSInteger linha = [indexPath row];
    NSString *tmp = [lista objectAtIndex:linha];
    [banco execSQL:[NSString stringWithFormat:@"delete from s4m_exercise where grupo = '%@';",tmp]];
    [lista removeObjectAtIndex:linha];
    [tableTreino reloadData];
     */
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger linha = [indexPath row];
    
     NSDictionary *tmp = [lista objectAtIndex:linha];
    [[Exercise exerciseCompartilhado] removerItem:[[[Exercise exerciseCompartilhado] itens] objectAtIndex:linha]];
    //[lista removeObjectAtIndex:linha];
    [tableTreino reloadData];
}

- (void)cellSwiped:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        UITableViewCell *cell = (UITableViewCell *)gestureRecognizer.view;
        NSIndexPath* indexPath = [self.tableTreino indexPathForCell:cell];
        if (DEBUG_LOG)  NSLog(@"Entrou cellSwiped ...");
    }
}

#pragma mark Table View Delegate Métodos
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"ENTRANDO NOS DETALHES DO TREINO");
    NSUInteger linha = [indexPath row];
    treinogeral = (int)linha;
    NSLog(@"PASSOU PARA COLETA");
    //treino_id = treinogeral.tid;
    exerc_detalhe = [[[Exercise exerciseCompartilhado] itens] objectAtIndex:linha];
    NSLog(@"MUTANDO TELA ...");
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    EditTrainerViewController *etcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"edittrainer_view"];
    [self presentViewController:etcontroller animated:YES completion:nil];
    //self.editController = etcontroller;
    //[self.view addSubview:etcontroller.view];
    //[self.navigationController pushViewController:etcontroller animated:YES];
}

-(void)getInfoTreino{
    //// MIGRAR PARA O COREDATA
    aTrainer = [[NSMutableArray alloc] init];
    aTrainer = [[Exercise exerciseCompartilhado] itens];
    if (DEBUG_LOG) NSLog(@"TREINOS RECUPERADOS -> %@", aTrainer);
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    //[self PopularListaTreino];
    lista = aTrainer;
    [[self tableTreino] reloadData];
}

-(IBAction)sendBack:(id) sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self dismissModalViewControllerAnimated:YES];
    //[self.view removeFromSuperview];
}

-(IBAction)sendAddTrainer:(id) sender {
    exerc_detalhe = nil;
    treinogeral = -1;
    EditTrainerViewController *etcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"edittrainer_view"];
    [self presentViewController:etcontroller animated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    if (DEBUG_LOG) NSLog(@"\n\n\nReload tableview ...\n\n");
    [self getInfoTreino];
    [tableTreino reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    if (DEBUG_LOG) NSLog(@"Movendo linha ...");
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    NSString *stringToMove = lista[sourceIndexPath.row];
    [lista removeObjectAtIndex:sourceIndexPath.row];
    [lista insertObject:stringToMove atIndex:destinationIndexPath.row];
}


@end
