//
//  Food.m
//  TotalTrainer
//
//  Created by Eduardo on 05/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "Food.h"

@implementation Food

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSError *erro = nil;
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarFoods];
    }
    return self;
}

+(Food *)foodCompartilhado {
    static Food *foodCompartilhado = nil;
    if (!foodCompartilhado) foodCompartilhado = [[super allocWithZone:nil] init];
    return foodCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self foodCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_Foods *)food{
    [itens addObject:food];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_Foods *)food {
    [contexto deleteObject:food];
    [itens removeObject:food];
    [self salvarMudancas];
}

-(void)carregarFoods {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Foods"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"type, category" ascending:YES];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta Foods" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(NSMutableArray *)carregarFoods:(NSString *)type {
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Foods"];
    [requisicao setEntity:entidade];
    NSLog(@"TYPE -> %@", type);
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"type CONTAINS %@ ",type];
    [requisicao setPredicate:predicate];
    NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"type" ascending:YES];
    [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta Foods" format:@"Motivo: %@", [erro localizedDescription]];
    NSMutableArray *listtotal =  [resultado copy];
    NSMutableArray *listaG = [[NSMutableArray alloc] init];
    for (int i = 0; [listtotal count] > i; i++) {
 
    }
    return listaG;
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarFoods];
    return sucess;
}

-(S4M_Foods *)criarFood:(NSString *)type setCategory:(NSString *)categoria setName:(NSString *)name setCode:(NSString *)code setCarbohydrates:(NSString *)carbohydrates setCalories:(NSString *)calories setProteins:(NSString *)proteins setSodio:(NSString *)sodio {
    S4M_Foods *food = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Foods" inManagedObjectContext:contexto];
    [food setType:type];
    [food setCategory:categoria];
    [food setName:name];
    [food setCode:code];
    [food setCarbohydrates:carbohydrates];
    [food setCalories:calories];
    [food setProteins:proteins];
    [food setSodio:sodio];
    [itens addObject:food];
    
    [self salvarMudancas];
    [self carregarFoods];
    return food;
}

-(S4M_Foods *)upgradeFood:(int)pos setType:(NSString *)type setCategory:(NSString *)categoria setName:(NSString *)name setCode:(NSString *)code setCarbohydrates:(NSString *)carbohydrates setCalories:(NSString *)calories setProteins:(NSString *)proteins setSodio:(NSString *)sodio {
    [self carregarFoods];
    S4M_Foods *food = [itens objectAtIndex:pos];
    [food setType:type];
    [food setCategory:categoria];
    [food setName:name];
    [food setCode:code];
    [food setCarbohydrates:carbohydrates];
    [food setCalories:calories];
    [food setProteins:proteins];
    [food setSodio:sodio];
    
    [self salvarMudancas];
    [self carregarFoods];
    return food;
}

-(void)excluirFoods:(S4M_Foods *)food {
    [contexto deleteObject:food];
    [itens removeObjectIdenticalTo:food];
}

@end
