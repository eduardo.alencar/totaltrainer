//
//  myTimerViewController.m
//  myTimer
//
//  Created by Eduardo on 20/08/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import "myTimerViewController.h"

@interface myTimerViewController ()

@end

@implementation myTimerViewController

@synthesize linterval, ltotal, ltitle_interval, ltitle_total;
@synthesize lcarga, lexercicio;
@synthesize lgrupo, lrepeticao;
@synthesize list;
@synthesize adBannerView;
@synthesize bfatController;
@synthesize trainerController;
@synthesize setupController;
@synthesize bavalicao, btrainer, blogout, bcontinue, bUp, bDown;
@synthesize imageGroup;
@synthesize locationManager;

@synthesize  beginTime;
@synthesize loggedInUser = _loggedInUser;
@synthesize slt;
@synthesize fliteController;
@synthesize bSetup;
@synthesize sidebarButton = _sidebarButton;

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Loading", @"") delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    [linterval setText:@"00 00"];
    [ltotal setText:@"00 00"];
    DEBUG_LOG = YES;
    if (DEBUG_LOG) NSLog(@"[myTimer]Iniciando o sistema do iCloud ......");
    
    NSUbiquitousKeyValueStore *store = [NSUbiquitousKeyValueStore defaultStore];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                          selector:@selector(storeDidChange:)
                                          name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                                          object:store];
        
    [[NSUbiquitousKeyValueStore defaultStore] synchronize];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                          selector:@selector(didAddNewNote:)
                                          name:@"New Note"
                                          object:nil];

    // nao deixa bloquear o aparelho
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    colorGray = [UIColor colorWithRed:0.22 green:0.27 blue:0.306 alpha:1];
    //rcolorRed = [UIColor colorWithRed:0.796 green:0.219 blue:0.15 alpha:1];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    if (IS_IPHONE_5) [[UIImage imageNamed:@"fundo-cronometro-02.png"] drawInRect:self.view.bounds];
    else [[UIImage imageNamed:@"fundo-cronometro-01.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [linterval setTextColor:colorGray];
    [linterval setFont:[UIFont fontWithName:@"DINEngschriftStd" size:110]];
    [ltotal setFont:[UIFont fontWithName:@"DINEngschriftStd" size:44]];
    [blogout setFont:[UIFont fontWithName:@"Exo-Medium" size:13]];
    [btrainer setFont:[UIFont fontWithName:@"Exo-Medium" size:13]];
    [bavalicao setFont:[UIFont fontWithName:@"Exo-Medium" size:13]];
    [bcontinue setFont:[UIFont fontWithName:@"Exo-Medium" size:16]];
    
    [lcarga setFont:[UIFont fontWithName:@"Exo-Medium" size:28]];
    [lcarga sizeToFit];
    [lrepeticao setFont:[UIFont fontWithName:@"Exo-Medium" size:28]];
    [lexercicio setFont:[UIFont fontWithName:@"Exo-Medium" size:15]];
    
    [lgrupo setFont:[UIFont fontWithName:@"Exo-Medium" size:15]];
    [ltitle_total setFont:[UIFont fontWithName:@"Exo-Medium" size:17]];
    [ltitle_interval setFont:[UIFont fontWithName:@"Exo-Medium" size:24]];
    
    [bavalicao setTitle:NSLocalizedString(@"princ_avaliacao", @"") forState:UIControlStateNormal];
    [btrainer setTitle:NSLocalizedString(@"princ_treino", @"") forState:UIControlStateNormal];
    [bcontinue setTitle:NSLocalizedString(@"princ_continue", @"") forState:UIControlStateNormal];
    [lexercicio setText:NSLocalizedString(@"princ_workout", @"")];
    [ltitle_interval setText:NSLocalizedString(@"princ_interval", @"")];
    [ltitle_total setText:NSLocalizedString(@"princ_totaltrainer", @"")];
    
    [bDown setBackgroundImage:[UIImage imageNamed:@"fundo-cronometro-add-off-01.png"] forState:UIControlStateSelected];
    [bDown setBackgroundImage:[UIImage imageNamed:@"fundo-cronometro-add-off-01.png"] forState:UIControlStateHighlighted];
    [bDown setBackgroundImage:[UIImage imageNamed:@"fundo-cronometro-add-on-01.png"] forState:UIControlStateNormal];
    [bUp setBackgroundImage:[UIImage imageNamed:@"fundo-cronometro-add-off-01.png"] forState:UIControlStateSelected];
    [bUp setBackgroundImage:[UIImage imageNamed:@"fundo-cronometro-add-off-01.png"] forState:UIControlStateHighlighted];
    [bUp setBackgroundImage:[UIImage imageNamed:@"fundo-cronometro-add-on-01.png"] forState:UIControlStateNormal];

    aalf = [NSArray arrayWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",nil];
    stop = YES;
    maxG = [[Exercise exerciseCompartilhado] MaxTrainer];
    maxG = maxG > 9 ? 9 : maxG;
    posTreino = 0;

    list = [[Setup setupCompartilhado] itens];
    if ([list count] > 0) {
        S4M_Setup *s4ms = [list objectAtIndex:0];
        if (DEBUG_LOG) NSLog(@"RECUPEROU COM SUCESSO OS VALORES ... -> %@", [s4ms speak]);
        //fbB =  [s4ms facebook] == 0 ? NO : YES;
        //SOUND = [s4ms sound] == 0 ? NO : YES;
        //SPEAK = [s4ms speak] == 0 ? NO : YES;
        fbB =  [s4ms facebook] == [NSNumber numberWithInt:0] ? FALSE : TRUE;
        SOUND = [s4ms sound] == [NSNumber numberWithInt:0] ? FALSE : TRUE;
        SPEAK = [s4ms speak] == [NSNumber numberWithInt:0] ? FALSE : TRUE;
        [linterval setText:[NSString stringWithFormat:@"%@ %@", [s4ms interval_min], [s4ms interval_sec]]];
        [ltotal setText:[NSString stringWithFormat:@"%@ %@", [s4ms total_hour], [s4ms total_min]]];
    } else {
        if (DEBUG_LOG) NSLog(@"ERRO NA RECUPERACAO DAS INFO DO SETUP");
    [linterval setText:[NSString stringWithFormat:@"%2i %2i", 0, 45]];
        [ltotal setText:[NSString stringWithFormat:@"%2i %2i", 0, 45]];

    }
    totsegs = thora = tmin = tsegs =  0;
    NSURL* file = [NSURL URLWithString:[[NSBundle mainBundle] pathForResource:@"alarm" ofType:@"mp3"]];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:nil];
    if (SOUND)[audioPlayer prepareToPlay];
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    countTotal = [NSTimer scheduledTimerWithTimeInterval:1.00 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    
    /// LOCALIZACAO VIA GPS
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = 0.0;
    /// DESLIGANDO ... [self.locationManager startUpdatingLocation];
    
    //fliteController = slt = nil;
    //fliteController = [[FliteController alloc] init];
    //slt = [[Slt alloc] init];
    //fliteController.duration_stretch = 2.0;
    //[fliteController say:@"Supino Reto com Halter" withVoice:slt];
    
    self.beginTime = [NSDate date];
    
    //[self updateListTrainer:@"A"];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    viewFront = self.view;
}

-(void)updateListTrainer:(NSString *)Day {
    listTrainer = [[Exercise exerciseCompartilhado] carregarExercise:Day];
    posTreino = 0;
    finalTreino = NO;
    if ([Day isEqualToString:@"A"]|| [Day isEqualToString:@"E"] || [Day isEqualToString:@"I"]) [imageGroup setImage:[UIImage imageNamed:@"fundo-cronometro-serie-a-01.png"]];
    else if ([Day isEqualToString:@"B"] || [Day isEqualToString:@"F"] || [Day isEqualToString:@"J"]) [imageGroup setImage:[UIImage imageNamed:@"fundo-cronometro-serie-b-01.png"]];
    else if ([Day isEqualToString:@"C"] || [Day isEqualToString:@"G"]) [imageGroup setImage:[UIImage imageNamed:@"fundo-cronometro-serie-c-01.png"]];
    else [imageGroup setImage:[UIImage imageNamed:@"fundo-cronometro-serie-d-01.png"]];
    if ([listTrainer count] > 0) {
        S4M_Exercise *list_treino = [listTrainer objectAtIndex:0];
        if (DEBUG_LOG) NSLog(@"FICHA -> [%@]", list_treino);
        lexercicio.text = [[NSString stringWithFormat:@"%@",[list_treino exercicio]] uppercaseString];
        lcarga.text = [NSString stringWithFormat:@"%@", [list_treino carga]];
        lgrupo.text = [[[[[[[[[[[list_treino grupo] stringByReplacingOccurrencesOfString:@"1" withString:@""] stringByReplacingOccurrencesOfString:@"2" withString:@""] stringByReplacingOccurrencesOfString:@"3" withString:@""] stringByReplacingOccurrencesOfString:@"4" withString:@""] stringByReplacingOccurrencesOfString:@"5" withString:@""] stringByReplacingOccurrencesOfString:@"6" withString:@""] stringByReplacingOccurrencesOfString:@"7" withString:@""] stringByReplacingOccurrencesOfString:@"8" withString:@""] stringByReplacingOccurrencesOfString:@"9" withString:@""] stringByReplacingOccurrencesOfString:@"0" withString:@""];
        lrepeticao.text = [list_treino repeticao];
        
        if (SPEAK) {
            fliteController = slt = nil;
            fliteController = [[FliteController alloc] init];
            slt = [[Slt alloc] init];
            fliteController.duration_stretch = 1.7;
            [fliteController say:[NSString stringWithFormat:@"NEXT Sets : %@  of %@. Weight : %@", [list_treino repeticao],  [list_treino exercicio], [list_treino carga]] withVoice:slt];
        }
    } else {
        [lexercicio setText:NSLocalizedString(@"princ_workout", @"")] ;
        [lrepeticao setText:@""];
        [lcarga setText:@""];
    }
}

-(void)viewDidUnload{
    [alert show];
    [[Setup setupCompartilhado] carregarSetup];
    list = [[Setup setupCompartilhado] itens];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.loggedInUser = nil;
    listTrainer = nil;
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

- (void) startCountDown:(int)sec {
    countDown = sec;
    [self formatTime];
    //NSLog(@"CONTADOR ...%d", countDown);
    linterval.text = [NSString stringWithFormat:@"%02i %02i", min, segs];
    linterval.textColor = colorGray;
    linterval.hidden = FALSE;
    if (!countDownTimer) {
        //countDownTimer = [NSTimer scheduledTimerWithTimeInterval:25.00 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        MachTimer* aTimer = [MachTimer timer];
        [self updateTime:aTimer];
        if (DEBUG_LOG) NSLog(@"performSomeOperation took %f seconds", [aTimer elapsedSeconds]);
    }
}

-(void)updateTrainer {
    if (posTreino < [listTrainer count]-1) {
        S4M_Exercise *treino = [listTrainer objectAtIndex:++posTreino];
        [lexercicio setText:[[treino exercicio] uppercaseString]];
        [lcarga setText:[NSString stringWithFormat:@"%@",[treino carga]]];
        [lrepeticao setText:[treino repeticao]];
        if (SPEAK) {
            fliteController = slt = nil;
            fliteController = [[FliteController alloc] init];
            slt = [[Slt alloc] init];
            fliteController.duration_stretch = 1.6;
            [fliteController say:[NSString stringWithFormat:@"NEXT Sets : %@  of %@. Weight : %@", [treino repeticao],  [treino exercicio], [treino carga]] withVoice:slt];
        }
    } else {
        finalTreino = YES;
        alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Finish",@"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [lexercicio setText:[[NSString stringWithFormat:NSLocalizedString(@"Finish",@"")] uppercaseString]];
        [lcarga setText:[NSString stringWithFormat:@""]];
        [lrepeticao setText:@""];
        //atualizar base
        maxG = [[Exercise exerciseCompartilhado] MaxTrainer];
        contG = (contG <= maxG) ? 0 : contG+1;
        
        if (SPEAK) {
            fliteController = slt = nil;
            fliteController = [[FliteController alloc] init];
            slt = [[Slt alloc] init];
            fliteController.duration_stretch = 3.0;
            [fliteController say:@"congratulations" withVoice:slt];
        }
 
        //atualizar FB
        if ([fbID length] > 0) [self updateFaceBook2];
        //if([rkID length] > 0)
            //[self updateRunKeeper];
    }
}

- (void)updateTime:(NSTimer *)timerParam {
    countDown--;
    //countTotal++;
    totsegs++;
    if(countDown == 0) {
        [self clearCountDownTimer];
        //do whatever you want after the countdown
        //VIBRAR
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        if (SOUND) {
            [audioPlayer play];
            sleep(5);
            [audioPlayer pause];
        }
        stop = YES;
        if ([listTrainer count] > 0) [self updateTrainer];
    }
    [self formatTime];
    //linterval.textColor = (countDown < 10) ? colorRed : colorGray;
    linterval.text = [NSString stringWithFormat:@"%02i %02i", min, segs];
    ltotal.text = [NSString stringWithFormat:@"00 %02i", tmin];
    //ltotal.text = [NSString stringWithFormat:@"00:%02i:%02i", tmin, tsegs];
}

-(void) clearCountDownTimer {
    [countDownTimer invalidate];
    countDownTimer = nil;
    //linterval.hidden = TRUE;
    min = hora = segs = 0;
}

-(void)formatTime {
    if (countDown > 0) {
        min = countDown / 60;
        segs = countDown % 60;
    }
    if (totsegs > 0) {
        if (tmin > 59) {
            thora++;
            tmin = tmin - 60;
        } else
        tmin = totsegs / 60;
        tsegs = totsegs % 60;
    }
}

-(IBAction)clickedConfig:(id)sender {
    //SetupViewController *abcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"setup_view"];;
    //[self presentViewController:abcontroller animated:YES completion:nil];
}

-(IBAction)clickedContinue:(id)sender {
    if (stop) {
        if (DEBUG_LOG) NSLog(@"Play ");
        //MIGRAR PARA COREDATA
        //list = [banco getTimes];
        list = [[Setup setupCompartilhado] getIntervaloGeral];
        int total = 0;
        @try {
            total = 60*[[list objectAtIndex:0] intValue] + [[list objectAtIndex:1] intValue];
        } @catch (NSException *e ) {
            total = 45;
        } @finally {}
        [self startCountDown:total];
    } else {
        [linterval setText:@"00 00"];
    }
    stop = stop ? NO : YES;
}

// ******************************************* iAd ********************************************** //
- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    [banner setHidden:NO];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    [banner setHidden:YES];
}

// ******************************************* GPS ********************************************** //
#pragma mark CLLocationDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"didUpdateLocation: %@", newLocation);
 }

// UIAlertView helper for post buttons
- (void)showAlert:(NSString *)message result:(id)result error:(NSError *)error {
    
    NSString *alertMsg;
    NSString *alertTitle;
    if (error) {
        alertTitle = @"Error";
        if (error.fberrorShouldNotifyUser ||
            error.fberrorCategory == FBErrorCategoryPermissions ||
            error.fberrorCategory == FBErrorCategoryAuthenticationReopenSession) {
            alertMsg = error.fberrorUserMessage;
        } else {
            alertMsg = @"Operation failed due to a connection problem, retry later.";
        }
    } else {
        NSDictionary *resultDict = (NSDictionary *)result;
        alertMsg = [NSString stringWithFormat:@"Successfully posted '%@'.", message];
        NSString *postId = [resultDict valueForKey:@"id"];
        if (!postId) {
            postId = [resultDict valueForKey:@"postId"];
        }
        if (postId) {
            alertMsg = [NSString stringWithFormat:@"%@\nPost ID: %@", alertMsg, postId];
        }
        alertTitle = @"Success";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - FBLoginViewDelegate
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    // first get the buttons set for login mode
    // "Post Status" available when logged on and potentially when logged off.  Differentiate in the label.
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user {
    self.loggedInUser = user;
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    // test to see if we can use the share dialog built into the Facebook application
    FBShareDialogParams *p = [[FBShareDialogParams alloc] init];
    p.link = [NSURL URLWithString:@"http://developers.facebook.com/ios"];
#ifdef DEBUG
    [FBSettings enableBetaFeatures:FBBetaFeaturesShareDialog];
#endif
    BOOL canShareFB = [FBDialogs canPresentShareDialogWithParams:p];
    BOOL canShareiOS6 = [FBDialogs canPresentOSIntegratedShareDialogWithSession:nil];
    
    self.loggedInUser = nil;
}

- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    // see https://developers.facebook.com/docs/reference/api/errors/ for general guidance on error handling for Facebook API
    // our policy here is to let the login view handle errors, but to log the results
    if (DEBUG_LOG) NSLog(@"FBLoginView encountered an error=%@", error);
}

#pragma mark -
// Convenience method to perform some action that requires the "publish_actions" permissions.
- (void) performPublishAction:(void (^)(void)) action {
    // we defer request for permission to post to the moment of post, then we check for the permission
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // if we don't already have the permission, then we request it now
        [FBSession.activeSession requestNewPublishPermissions:@[@"publish_actions"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                if (!error) {
                                                    action();
                                                }
                                                //For this example, ignore errors (such as if user cancels).
                                            }];
    } else {
        action();
    }
}
 
-(IBAction)TrainerClick:(UIButton *)sender {
    TrainerTableViewController *ttcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"trainer_view"];;
    //self.trainerController = ttcontroller;
    //[self.view addSubview:ttcontroller.view];
    [self presentViewController:ttcontroller animated:YES completion:nil];
}

-(IBAction)BFClick:(UIButton *)sender {
    BodyFatViewController *bfcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"bodyfat_view"];;
    self.bfatController = bfcontroller;
    [self.view addSubview:bfcontroller.view];
}

-(IBAction)BUpClick:(UIButton *)sender {
    if (contG < maxG){
        contG++;
    [lgrupo setText:[aalf objectAtIndex:contG]];
    [self updateListTrainer:[aalf objectAtIndex:contG]];
    }
}

-(IBAction)BDownClick:(UIButton *)sender {
    if (contG > 0) {
        contG--;
        [lgrupo setText:[aalf objectAtIndex:contG]];
        [self updateListTrainer:[aalf objectAtIndex:contG]];
    }
}

-(void)updateFaceBook2 {
    //[CLController.locMgr startUpdatingLocation];
    //for (int i = 0; i < 1000; i++) {}
    //[CLController.locMgr stopUpdatingLocation];
    
    //MIGRAR PARA COREDATA
    //listTrainer = [banco getTrainerFB:[lgrupo text]];
    [self performPublishAction:^{
        NSString *message = [NSString stringWithFormat:@"TotalTrainer :  %@ \n", [ltotal text]];//self.loggedInUser.first_name, [NSDate date]];
        for (int i = 0; i < [listTrainer count]; i++) {
            treino = [listTrainer objectAtIndex:i];
            message = [NSString stringWithFormat:@"%@ \n Sets : %@  of %@ , repss %@", message, treino.repeticao,  treino.exercicio, treino.carga];
        }
        [FBRequestConnection startForPostStatusUpdate:message
                                    completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {}];
    }];
}


-(void)logoutClick:(UIButton *)sender {
    //MIGRAR PARA COREDATA
    //[banco atualizarFB:@""];
    [self.view removeFromSuperview];
}

#pragma mark -
#pragma mark View Will/Did Appear
-(void)viewWillAppear:(BOOL)animated {
    if (DEBUG_LOG) NSLog(@"\n\nReload info ...\n\n");
    maxG = [[Exercise exerciseCompartilhado] MaxTrainer];
    maxG = maxG > 9 ? 9 : maxG;
    [self updateListTrainer:@"A"];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
}

#pragma mark -
#pragma mark View Will/Did Disappear
- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
}


#pragma mark -
#pragma mark Button Actions
-(IBAction)btnMovePanelLeft:(id)sender {
	UIButton *button = sender;
	switch (button.tag) {
		case 0: {
			[_delegate movePanelToOriginalPosition];
			break;
		}
		case 1: {
			[_delegate movePanelLeft];
			break;
		}
		default:
			break;
	}
}

@end
