//
//  S4M_Version.h
//  TotalTrainer
//
//  Created by Eduardo on 29/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_Version : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * version;

@end
