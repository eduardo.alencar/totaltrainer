//
//  Perfil.h
//  TotalTrainer
//
//  Created by Eduardo on 04/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S4M_Perfil.h"
#import "Global.h"

@interface Perfil : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(Perfil *)perfilCompartilhado;
-(NSMutableArray *)itens;
-(void)adicionarItem:(NSString *)perfil;
-(BOOL)salvarMudancas;
-(S4M_Perfil *)criarPerfil:(NSString *)fbID setUsername:(NSString *)username setName:(NSString *)name setGender:(NSString *)gender setBirthday:(NSDate *)day setCity:(NSString *)city setEmail:(NSString *)email setPicture:(NSString *)photo;
-(S4M_Perfil *)upgradePerfil:(NSString *)fbID setUsername:(NSString *)username setName:(NSString *)name setGender:(NSString *)gender setBirthday:(NSDate *)day setCity:(NSString *)city setEmail:(NSString *)email setPicture:(NSString *)photo;
-(void)carregarPerfil;
-(void)migrarPerfil :(NSMutableArray *)Info;

@end
