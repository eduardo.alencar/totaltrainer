//
//  BodyFatViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 01/07/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface BodyFatViewController : UIViewController<UITextFieldDelegate>{
    UITextField *tidade, *tpeso, *tpeito, *tabdomen, *tcoxa;
    UIButton *bcalcule, *bsexo;
    UIAlertView *alert; //, *av_age, *av_weight, *av_abdominal, *av_thigh, *av_chest;
    UILabel *ltitle, *lsexo, *lfem, *lmas, *lidade, *lpeso, *lpesoun, *ltitle_dobra, *labdomen, *labdomenun, *lcoxa, *lcoxaun, *lpeitoral, *lpeitoralun;
    Boolean masc;
    double densidade, bf, soma, peso, imc, siri, coxa, peito, abdomen;
    int idade;
    BodyFat *BF;
}

@property(nonatomic, retain) IBOutlet UITextField *tidade, *tpeso, *tpeito, *tabdomen, *tcoxa;
@property(retain, nonatomic) IBOutlet UIButton *bcalcule, *bsexo;
@property(retain, nonatomic) IBOutlet UILabel *ltitle, *lsexo, *lfem, *lmas, *lidade, *lpeso, *lpesoun, *ltitle_dobra, *labdomen, *labdomenun, *lcoxa, *lcoxaun, *lpeitoral, *lpeitoralun;

-(NSString *)CalBodyFat;
-(IBAction)calcBF:(id) sender;
-(IBAction)back:(id) sender;
-(IBAction)getAge:(id)sender;
-(IBAction)getWeight:(id)sender;
-(IBAction)getAbdominal:(id)sender;
-(IBAction)getPeito:(id)sender;
-(IBAction)getCoxa:(id)sender;
-(IBAction)buttonTap:(id)sender;

@end
