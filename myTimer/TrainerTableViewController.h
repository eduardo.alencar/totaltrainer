//
//  TrainerTableViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 29/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "Trainer.h"
#import "EditTrainerViewController.h"
#import "Exercise.h"
#import "S4M_Exercise.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@class myTimerViewController;

@interface TrainerTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    UITableViewCell *tableViewCelula;
    UITableView *tableTreino;
    UILabel *lpos, *lexerc, *lrepeat, *llbs, *ltitle;
    UIAlertView *alert;
    NSMutableArray *lista;
    Trainer *treino;
    EditTrainerViewController *editController;
    IBOutlet myTimerViewController *myTimerController;
}

@property(nonatomic, retain) IBOutlet UITableViewCell *tableViewCelula;
@property(nonatomic, retain) IBOutlet UITableView *tableTreino;
@property(nonatomic, retain) IBOutlet UILabel *lpos, *lexerc, *lrepeat, *llbs, *ltitle;
@property(nonatomic, retain) EditTrainerViewController *editController;

//-(void)PopularListaTreino;
-(void)getInfoTreino;
-(void)reloadTableView;
-(IBAction)sendBack:(id) sender;
-(IBAction)sendAddTrainer:(id) sender;

@end
