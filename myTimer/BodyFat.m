//
//  BodyFat.m
//  TotalTrainer
//
//  Created by Eduardo on 01/07/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "BodyFat.h"

@implementation BodyFat

-(id)init {
    self = [super init];
    if (self) {
        siri = bf = densidade = 0.0;
    }
    return self;
}

-(double)JacksonPollock3Dobras:(double)Sum setAge:(double)Age setGender:(NSString *)gender {
    if ([gender isEqualToString:@"M"]) densidade = 1.10938 - (0.0008267 * Sum) + ( 0.0000016 * Sum * Sum ) - 0.0002574*Age;
    else densidade = 1.0994921 - (0.0009929 * Sum) + (0.0000023 * Sum * Sum) - 0.0001392 * Age;
    //if (DEBUG_LOG) NSLog(@"Densidade -> %f", densidade);
    return (495/densidade)-450;
}

-(double)IMCUSP2012:(double)kg setBF:(double)fat setH:(int)cm  {
    //1.35 a 1.65 => abaixo do peso
    //1.65 a 2.00 => normal
    //2.00 a + =>  obesidade
    return (kg > 1 && bf > 0 && cm > 1) ? (3*kg+4*bf)/cm : 0;
}

@end
