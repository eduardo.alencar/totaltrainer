//
//  EditTrainerViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 30/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@class TrainerTableViewController;

@interface EditTrainerViewController : UIViewController<UITextFieldDelegate> {
    IBOutlet UITextField *texer, *tgrupo, *trepeticao, *tserie, *tpeso;
    UILabel *ltitle, *lgroup, *lexer, *lserie, *lrepeticao, *lpeso, *lhelp1, *lhelp2;
    UIButton *bcalcule;
    IBOutlet TrainerTableViewController *trainerViewController;
    //UIAlertView *av_exer, *av_grupo, *av_peso, *av_repeticao, *av_serie;
    Trainer *treino;
    NSMutableArray *lista;
    UIAlertView *alert;
    int iserie;
    NSString *sexer, *sgrupo, *srepeticao, *speso, *stemp, *sserie, *sql;
}

@property(nonatomic, retain) IBOutlet UITextField *texer, *tgrupo, *tserie, *trepeticao, *tpeso;
@property(nonatomic, retain) IBOutlet UIButton *bsave, *bcancel;
@property(nonatomic, retain) IBOutlet UILabel *ltitle, *lgroup, *lexer, *lserie, *lrepeticao, *lpeso, *lhelp1, *lhelp2;
@property(nonatomic, retain) IBOutlet UIButton *bcalcule;

-(IBAction)savedTrainer:(id) sender;
-(IBAction)cancelTrainer:(id) sender;
-(IBAction)editGroup:(id) sender;
-(IBAction)editSeries:(id) sender;
-(IBAction)editExercise:(id) sender;
-(IBAction)editRepeat:(id) sender;
-(IBAction)editWeight:(id) sender;

@end
