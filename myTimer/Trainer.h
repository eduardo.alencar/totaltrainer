//
//  Trainer.h
//  TotalTrainer
//
//  Created by Eduardo on 29/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trainer : NSObject {
    NSString *exercicio, *grupo, *repeticao, *carga, *tid;
}

@property(retain, nonatomic) NSString *exercicio, *grupo, *repeticao, *carga ,*tid;

@end
