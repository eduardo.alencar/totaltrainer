//
//  main.m
//  myTimer
//
//  Created by Eduardo on 20/08/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myTimerAppDelegate.h"

int main(int argc, char *argv[]){
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([myTimerAppDelegate class]));
    }
}
