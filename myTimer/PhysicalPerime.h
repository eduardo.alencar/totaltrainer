//
//  PhysicalPerime.h
//  TotalTrainer
//
//  Created by Eduardo on 08/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Global.h"
#import "S4M_PhysicalPerime.h"

@interface PhysicalPerime : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(PhysicalPerime *)physicalPerimeCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(S4M_PhysicalPerime *)physicalPerime;
-(void)removerItem:(S4M_PhysicalPerime *)physicalPerime;
-(BOOL)salvarMudancas;
-(S4M_PhysicalPerime *)criarPhysicalperime:(NSString *)physicalplan_id setAbdomen:(NSNumber *)abdomen setAntebraco:(NSNumber *)antebraco setBiceps:(NSNumber *)biceps  setCintura:(NSNumber *)cintura setCoxa:(NSNumber *)coxa setPanturrilha:(NSNumber *)panturrilha setPeitoral:(NSNumber *)peitoral setQuadril:(NSNumber *)quadril;
-(S4M_PhysicalPerime *)upgradePhysicalperime:(int)pos setPlan:(NSString *)physicalplan_id setAbdomen:(NSNumber *)abdomen setAntebraco:(NSNumber *)antebraco setBiceps:(NSNumber *)biceps  setCintura:(NSNumber *)cintura setCoxa:(NSNumber *)coxa setPanturrilha:(NSNumber *)panturrilha setPeitoral:(NSNumber *)peitoral setQuadril:(NSNumber *)quadril;
-(void)excluirPhysicalperime:(S4M_PhysicalPerime *)physicalperime;
-(void)carregarPhysicalperime;
-(NSMutableArray *)carregarPhysicalperime:(NSString *)plan_id;

@end
