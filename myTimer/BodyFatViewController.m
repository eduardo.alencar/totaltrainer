//
//  BodyFatViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 01/07/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "BodyFatViewController.h"

@interface BodyFatViewController ()

@end

@implementation BodyFatViewController

@synthesize bcalcule, tpeito, tabdomen, tidade, tcoxa, tpeso;
@synthesize bsexo;
@synthesize ltitle, lsexo, lfem, lmas, lidade, lpeso, lpesoun, ltitle_dobra, labdomen, labdomenun, lcoxa, lcoxaun, lpeitoral, lpeitoralun;

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Loading",@"") delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    
    [tidade setDelegate:self];
    [tpeso setDelegate:self];
    [tpeito setDelegate:self];
    [tcoxa setDelegate:self];
    [tabdomen setDelegate:self];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    if (IS_IPHONE_5) [[UIImage imageNamed:@"fundo-massa-corporal-02.png"] drawInRect:self.view.bounds];
    else [[UIImage imageNamed:@"fundo-massa-corporal-01.png"] drawInRect:self.view.bounds];

    [[UIImage imageNamed:@""] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [bsexo setImage:[UIImage imageNamed:@"bullet-direita-01.png"] forState:UIControlStateSelected];
    [bsexo setImage:[UIImage imageNamed:@"bullet-esquerda-01.png"] forState:UIControlStateNormal];
    
    [ltitle setFont:[UIFont fontWithName:@"DINEngschriftStd" size:26]];
    [bcalcule setFont:[UIFont fontWithName:@"Exo-Medium" size:16]];
   
    masc = YES;
    [bsexo setSelected:masc];
    
    [ltitle setText:NSLocalizedString(@"bf_bodymass",@"")];
    [lsexo setText:NSLocalizedString(@"bf_sex",@"")];
    [lfem setText:NSLocalizedString(@"bf_fem",@"")];
    [lmas setText:NSLocalizedString(@"bf_male",@"")];
    [lidade setText:NSLocalizedString(@"bf_age",@"")];
    [lpeso setText:NSLocalizedString(@"bf_peso",@"")];
    [ltitle_dobra setText:NSLocalizedString(@"bf_dobra",@"")];
    [labdomen setText:NSLocalizedString(@"bf_abdomen",@"")];
    [lcoxa setText:NSLocalizedString(@"bf_coxa",@"")];
    [lpeitoral setText:masc ? NSLocalizedString(@"bf_peito",@"") : NSLocalizedString(@"bf_triceps",@"")];
    
    [bcalcule setTitle:NSLocalizedString(@"bf_calculate",@"") forState:UIControlStateNormal];
    
    densidade = bf = soma = idade = siri = peso = imc = coxa = abdomen = peito = 0;
    
    BF = [[BodyFat alloc] init];
    if (DEBUG_LOG) NSLog(@"Iniciaçao  OK ... ");
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)calcBF:(id) sender {
    masc = [bsexo isSelected];
    NSString *tmp;
    @try {
        idade = [[tidade text] integerValue];
     } @catch (NSException *e) {
        idade = 0;
    }
    @try {
        tmp = [[tpeso text] stringByReplacingOccurrencesOfString:@"," withString:@"."];
        peso = [tmp doubleValue];
    } @catch (NSException *e) {
        peso = 0;
    }
    @try {
        tmp = [[tcoxa text] stringByReplacingOccurrencesOfString:@"," withString:@"."];
        coxa = [tmp doubleValue];
    } @catch (NSException *e) {
        coxa = 0;
    }
    @try {
        tmp = [[tabdomen text] stringByReplacingOccurrencesOfString:@"," withString:@"."];
        abdomen = [tmp doubleValue];
    } @catch (NSException *e) {
        abdomen = 0;
    }
    @try {
        tmp = [[tpeito text] stringByReplacingOccurrencesOfString:@"," withString:@"."];
        peito = [tmp doubleValue];
    } @catch (NSException *e) {
        peito = 0;
    }
    if (idade < 12 || idade > 99) alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"bf_errorage",@"") delegate:nil cancelButtonTitle:@"OK"otherButtonTitles: nil];
    else if (peso < 10) alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"bf_errorweight",@"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    else if (abdomen < 0.09) alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"bf_errorabdominal","") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    else if (peito < 0.09) alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"bf_errorchest", @"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    else if (coxa < 0.09) alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"bf_errorthigh",@"") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    else {
        soma = coxa + abdomen + peito;
        soma = [BF JacksonPollock3Dobras:soma setAge:idade setGender:(masc ? @"M" : @"F")];
        alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:[NSString stringWithFormat:NSLocalizedString(@"bf_bf",@""), soma] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    [alert show];
}

-(IBAction)back:(id) sender {
   [self.view removeFromSuperview]; 
}

-(IBAction)getAge:(id)sender{
    /*
    av_age = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"bf_age2", @"") message:@"Age" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:@"OK", nil];
    tbfAlertIdade = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [tbfAlertIdade setBackgroundColor:[UIColor whiteColor]];
    [tbfAlertIdade setTextAlignment:UITextAlignmentCenter];
    [tbfAlertIdade setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [av_age addSubview:tbfAlertIdade];
    [av_age show];
     */
}

-(IBAction)getWeight:(id)sender{
    /*
    av_weight = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"bf_weight2", @"new_list_dialog") message:@"Weight" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:@"OK", nil];
    tbfAlertPeso = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [tbfAlertPeso setBackgroundColor:[UIColor whiteColor]];
    [tbfAlertPeso setTextAlignment:UITextAlignmentCenter];
    [tbfAlertPeso setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [av_weight addSubview:tbfAlertPeso];
    [av_weight show];
     */
}

-(IBAction)getAbdominal:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    self.view.frame = CGRectMake(0,-60, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];

    /*
    av_abdominal = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"bf_abdominal2", @"") message:@"Abdominal" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:@"OK", nil];
    tbfAlertAbdomen = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [tbfAlertAbdomen setBackgroundColor:[UIColor whiteColor]];
    [tbfAlertAbdomen setTextAlignment:UITextAlignmentCenter];
    [tbfAlertAbdomen setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [av_abdominal addSubview:tbfAlertAbdomen];
    [av_abdominal show];
     */
}

-(IBAction)getPeito:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    self.view.frame = CGRectMake(0,-140, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];

    /*
    av_chest = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"bf_chest2", @"") message:@"Chest" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:@"OK", nil];
    tbfAlertPeito = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [tbfAlertPeito setBackgroundColor:[UIColor whiteColor]];
    [tbfAlertPeito setTextAlignment:UITextAlignmentCenter];
    [tbfAlertPeito setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [av_chest addSubview:tbfAlertPeito];
    [av_chest show];
     */
}

-(IBAction)getCoxa:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    self.view.frame = CGRectMake(0,-100, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    /*
    av_thigh = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"bf_thigh2", @"new_list_dialog") message:@"Thigh" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:@"OK", nil];
    tbfAlertCoxa = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [tbfAlertCoxa setBackgroundColor:[UIColor whiteColor]];
    [tbfAlertCoxa setTextAlignment:UITextAlignmentCenter];
    [tbfAlertCoxa setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [av_thigh addSubview:tbfAlertCoxa];
    [av_thigh show];
    */
    
}

/*
-(void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet == av_abdominal) [tabdomen setText:[tbfAlertAbdomen text]];
    else if (actionSheet == av_age) [tidade setText:[tbfAlertIdade text]];
    else if (actionSheet == av_chest) [tpeito setText:[tbfAlertPeito text]];
    else if (actionSheet == av_thigh) [tcoxa setText:[tbfAlertCoxa text]];
    else [tpeso setText:[tbfAlertPeso text]];
    if (DEBUG_LOG) NSLog(@"IDADE =%@ / %@ / %@ / %@ / %@ / %c",[tbfAlertIdade text], [tcoxa text], [tpeito text], [tabdomen text], [tbfAlertPeso text], masc);
}
*/
 
-(IBAction)buttonTap:(id)sender {
    bsexo.selected = !bsexo.selected;
    masc = [bsexo isSelected];
    [lpeitoral setText:masc ? NSLocalizedString(@"bf_peito",@"") : NSLocalizedString(@"bf_triceps",@"")];
}


- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    //NSLog(@"Lendo ShouldReturn ...");
    if (theTextField == tidade) {
        //[theTextField becomeFirstResponder];
        [tpeso becomeFirstResponder];
    } else if (theTextField == tpeso) {
        [theTextField becomeFirstResponder];
        [tabdomen becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-60, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else if (theTextField == tabdomen) {
        [tcoxa becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-100, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else if (theTextField == tcoxa) {
        [tpeito becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-140, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
    return YES;
}

@end
