//
//  S4M_Setup.m
//  TotalTrainer
//
//  Created by Eduardo on 29/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "S4M_Setup.h"


@implementation S4M_Setup

@dynamic id;
@dynamic facebook;
@dynamic runkeeper;
@dynamic login;
@dynamic pass;
@dynamic gps;
@dynamic total_hour;
@dynamic total_min;
@dynamic interval_min;
@dynamic interval_sec;
@dynamic unit;
@dynamic grupo;
@dynamic sound;
@dynamic speak;

@end
