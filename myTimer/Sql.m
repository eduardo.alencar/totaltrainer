//
//  Sql.m
//  CadeEncomenda
//
//  Created by LebaS on 14/01/12.
//  Copyright 2012 Solution4Mac. All rights reserved.
//

#import "Sql.h"


@implementation Sql

@synthesize todosTimes;

-(id)init {
	self = [super init];
	if (self) {
		todosTimes = [[NSMutableArray alloc] init];
        list_trainer = [[NSMutableArray alloc] init];
		if (![self openDB]) [self createDB];
        else [self upgradeVersion];
	}
	return self;
}

-(NSString *)filePath {
	NSArray *path = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	documentDirectory = [[path objectAtIndex:0] stringByAppendingPathComponent: @"TotalTrainer"];
	return [documentDirectory stringByAppendingPathComponent:@"totaltrainer.s4m"];
}

-(BOOL)openDB{
	if (sqlite3_open([[self filePath] UTF8String],&database) != SQLITE_OK) {
		sqlite3_close(database);
	} else {
        printf("ABRIU O BANCO COM SUCESSO\n  path %s \n", [[self filePath] UTF8String]);
        return YES;
    }
    return NO;
}

-(void)MigrarSQL {
    //MIGRAR BASE
    NSLog(@" ==========> MIGRANDO DADOS PARA COREDATA ......");
    NSMutableArray *treinos = [self getTrainer];
    if ([treinos count] > 0) {
        NSLog(@" ==========> MIGRANDO DADOS DA VERSAO ......");
        [[Version versionCompartilhado] migrarVersion];
        NSLog(@" ==========> MIGRANDO DADOS DO SETUP ......");
        todosTimes = [self getTimes];
        //[[Setup setupCompartilhado] migrarSetup:todosTimes];
        NSLog(@" ==========> MIGRANDO DADOS DO TRAINER ......");
        [[Exercise exerciseCompartilhado] migrarExercise:[self getTrainer]];
    }
}

-(void)upgradeVersion {
    //if (DEBUG_LOG) NSLog(@"UPGRADE VERSION ...");
    const char *sql = "select version from s4m_setup where id = 1;";
	sqlite3_stmt  *statement;
	if (sqlite3_prepare_v2(database, sql, -1, &statement, nil) == SQLITE_OK){
		while (sqlite3_step(statement) == SQLITE_ROW) {
            NSString *version = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            if (![version isEqual:@"2.1.0"]) {
                [self MigrarSQL];
                /*
                //if (DEBUG_LOG) NSLog(@"ATUALIZANDO VERSION - > %@", version);
                NSString *csql = [NSString stringWithFormat:@"update s4m_setup set version = '%@' where id = 1;", @"2.0.4"];
                [self execSQL :csql];
                csql = @"alter table s4m_setup add sound text;";
                [self execSQL :csql];
                csql = @"update s4m_setup set sound = 'YES' where id = 1;";
                [self execSQL :csql];
                csql = @"alter table s4m_setup add speak text;";
                [self execSQL :csql];
                csql = @"update s4m_setup set speak = 'YES' where id = 1;";
                [self execSQL :csql];
            } else {
                //if (DEBUG_LOG) NSLog(@"VERSION %@", version);
                NSString *csql = [NSString stringWithFormat:@"DROP TABLE s4m_version;"];
                [self execSQL:csql];
                csql = [NSString stringWithFormat:@"DROP TABLE s4m_setup;"];
                [self execSQL:csql];
                csql = [NSString stringWithFormat:@"DROP TABLE s4m_exercise;"];
                [self execSQL:csql];
                [self createDB];
                 */
            }
        }
    } else [self MigrarSQL];//else [self createDB];
}

-(void)createDB {
    NSFileManager *fileManager= [NSFileManager defaultManager]; 
    if(![fileManager createDirectoryAtPath:documentDirectory withIntermediateDirectories:YES attributes:nil error:NULL])
        NSLog(@"Error: Create folder failed %@", documentDirectory);
    else {
        if (sqlite3_open([[self filePath] UTF8String],&database) == SQLITE_OK) {
            NSString *sql = [NSString stringWithFormat:@"CREATE TABLE s4m_setup ( id integer primary key autoincrement, facebook text, login text, pass text, gps text, total_hour text, total_min text, interval_min text, interval_sec text, unit text, grupo int, sound text, speak text);"];
            [self execSQL:sql];
            sql = [NSString stringWithFormat:@"CREATE TABLE s4m_version (id integer primary key autoincrement, version varchar(15) NOT NULL);"];
            [self execSQL:sql];
            sql = [NSString stringWithFormat:@"INSERT INTO s4m_version(id, version) VALUES(1,'2.0.4');"];
            [self execSQL:sql];
            sql = [NSString stringWithFormat:@"INSERT INTO s4m_setup VALUES(1,'','' , '', 'NO','00','45', '00', '45', 'kg','A', 'YES', 'NO');"];
            [self execSQL:sql];
            sql = [NSString stringWithFormat:@"CREATE TABLE sqlite_sequence(name,seq);"];
            [self execSQL:sql];
            sql = [NSString stringWithFormat:@"INSERT INTO sqlite_sequence VALUES('s4m_setup',7);"];
            [self execSQL:sql];
            sql = [NSString stringWithFormat:@"INSERT INTO sqlite_sequence VALUES('s4m_version',1);"];
            [self execSQL:sql];
            
            sql = [NSString stringWithFormat:@"CREATE TABLE s4m_exercise (id integer primary key autoincrement, grupo varchar(5) NOT NULL, exercicio varchar(255) NOT NULL, repeticao varchar(100), carga varchar(100));"];
            [self execSQL:sql];

            sql = [NSString stringWithFormat:@"COMMIT;"];
            [self execSQL:sql];
            sqlite3_close(database);
            [self openDB];
        }
    }
}

-(void)execSQL: (NSString *)tmp {
	sqlite3_stmt  *statement;
	const char *sql = [tmp UTF8String];
	if (sqlite3_prepare_v2(database, sql, -1, &statement, nil) != SQLITE_OK) NSLog(@"ERRO NO EXECSQL ... \n ===> [ code error %i ]",sqlite3_prepare_v2(database, sql, -1, &statement, nil));
	if (SQLITE_DONE != sqlite3_step(statement)) NSLog(@"ERRO NO INSERT / COMMIT\n  ===> [ code error %i ]",sqlite3_step(statement));
	printf("SQL -> TMP %s \n", sql);
	sqlite3_finalize(statement);
}

//========================= SETUP
-(NSMutableArray*)getTimes {
	const char *sql = "select facebook, login, pass, gps, total_hour, total_min, interval_min, interval_sec, grupo, sound, speak from s4m_setup where id = 1;";
	[todosTimes removeAllObjects];
	sqlite3_stmt  *statement;
	if (sqlite3_prepare_v2(database, sql, -1, &statement, nil) == SQLITE_OK){
		while (sqlite3_step(statement) == SQLITE_ROW) {
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]];
			[todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)]];
            [todosTimes addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)]];
		} 	
	} //else if (DEBUG_LOG)NSLog(@"===> ERRO NA CONSULTA III %@ \n", sql);
	sqlite3_finalize(statement);
    //if (DEBUG_LOG)  NSLog(@"SETUP -> %@",todosTimes);
	return [todosTimes copy];
}

-(NSMutableArray*)getTrainer {
	const char *sql = "select grupo, exercicio, repeticao, carga, id from s4m_exercise order by grupo;";
	[list_trainer removeAllObjects];
	sqlite3_stmt  *statement;
    Trainer *trainer;
    NSString *tmp;
	if (sqlite3_prepare_v2(database, sql, -1, &statement, nil) == SQLITE_OK){
		while (sqlite3_step(statement) == SQLITE_ROW) {
            trainer = [[Trainer alloc] init];
            trainer.grupo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
			trainer.exercicio = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            tmp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            trainer.repeticao = tmp;
            tmp = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            trainer.carga = tmp;
            trainer.tid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            [list_trainer addObject:trainer];
		}
	} else printf("===> ERRO NA CONSULTA II \n");
	sqlite3_finalize(statement);
	return [list_trainer copy];
}

-(NSMutableArray*)getTrainer:(NSString *)Day {
	NSString *srepeticao, *speso, *tmp = [NSString stringWithFormat:@"select grupo, exercicio, repeticao, carga from s4m_exercise where grupo like '%@%%' order by grupo;",Day];
    const char *sql = [tmp UTF8String];
    //if (DEBUG_LOG) NSLog(@"%s",sql);
    NSMutableArray *listG = [[NSMutableArray alloc] init];
	sqlite3_stmt  *statement;
	Trainer *trainer;
    if (sqlite3_prepare_v2(database, sql, -1, &statement, nil) == SQLITE_OK){
        while (sqlite3_step(statement) == SQLITE_ROW) {
            trainer = [[Trainer alloc] init];
            trainer.grupo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            trainer.exercicio = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            trainer.repeticao = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            trainer.carga = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            
            NSArray *arep = [trainer.repeticao componentsSeparatedByString:@"/"];
            NSArray *apeso = [trainer.carga componentsSeparatedByString:@"/"];
            for (int i = 0; i < [arep count]; i++) {
                trainer.carga = ([apeso count] > 1 ? [apeso objectAtIndex:i] : trainer.carga);
                trainer.repeticao = [arep objectAtIndex:i];
                [listG addObject:trainer];
            }
        }
	} //else if (DEBUG_LOG) NSLog(@"===> ERRO NA CONSULTA -> %@ \n", tmp);
	sqlite3_finalize(statement);
	return [listG copy];
}

-(NSMutableArray*)getTrainerFB:(NSString *)Day {
	NSString *srepeticao, *speso, *tmp = [NSString stringWithFormat:@"select grupo, exercicio, repeticao, carga from s4m_exercise where grupo like '%@%%' order by grupo;",Day];
    const char *sql = [tmp UTF8String];
    //if (DEBUG_LOG) NSLog(@"%s",sql);
    NSMutableArray *listG = [[NSMutableArray alloc] init];
	sqlite3_stmt  *statement;
	Trainer *trainer;
    if (sqlite3_prepare_v2(database, sql, -1, &statement, nil) == SQLITE_OK){
        while (sqlite3_step(statement) == SQLITE_ROW) {
            trainer = [[Trainer alloc] init];
            trainer.grupo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            trainer.exercicio = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            trainer.repeticao = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            trainer.carga = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            [listG addObject:trainer];
        }
	} //else if (DEBUG_LOG) NSLog(@"===> ERRO NA CONSULTA -> %@ \n", tmp);
	sqlite3_finalize(statement);
	return [listG copy];
}

-(void)atualizarTimes: (NSMutableArray *)lTimes {
	if ([lTimes count] == 11) {
		//update
        //if (DEBUG_LOG) NSLog(@"Salvando SETUP ....");
		NSString *sql = [NSString stringWithFormat:@"update s4m_setup set facebook = '%@' where id = 1;", [lTimes objectAtIndex:0]];
		[self execSQL :sql];
		
        sql = [NSString stringWithFormat:@"update s4m_setup set login = '%@' where id = 1;", [lTimes objectAtIndex:1]];
		[self execSQL :sql];
        
        sql = [NSString stringWithFormat:@"update s4m_setup set pass = '%@' where id = 1;", [lTimes objectAtIndex:2]];
		[self execSQL :sql];
        
        sql = [NSString stringWithFormat:@"update s4m_setup set gps = '%@' where id = 1;", [lTimes objectAtIndex:3]];
		[self execSQL :sql];
        
        sql = [NSString stringWithFormat:@"update s4m_setup set total_hour = '%@' where id = 1;", [lTimes objectAtIndex:4]];
        [self execSQL :sql];
        
        sql = [NSString stringWithFormat:@"update s4m_setup set total_min = '%@' where id = 1;", [lTimes objectAtIndex:5]];
		[self execSQL :sql];
		
        sql = [NSString stringWithFormat:@"update s4m_setup set interval_min = '%@' where id = 1;", [lTimes objectAtIndex:6]];
		[self execSQL :sql];
        
        sql = [NSString stringWithFormat:@"update s4m_setup set interval_sec = '%@' where id = 1;", [lTimes objectAtIndex:7]];
		[self execSQL :sql];
        
        [self atualizarGrupo:[lTimes objectAtIndex:8]];
        
        sql = [NSString stringWithFormat:@"update s4m_setup set sound = '%@' where id = 1;", [lTimes objectAtIndex:9]];
		[self execSQL :sql];
        
        sql = [NSString stringWithFormat:@"update s4m_setup set speak = '%@' where id = 1;", [lTimes objectAtIndex:10]];
		[self execSQL :sql];
	}
}

-(void)atualizarGrupo: (int)grupo {
    NSString *sql = [NSString stringWithFormat:@"update s4m_setup set grupo = %i where id = 1;", grupo];
    [self execSQL :sql];
}

-(void)atualizarFB:(NSString *)fbid {
    NSString *sql = [NSString stringWithFormat:@"update s4m_setup set facebook = '%@' where id = 1;", fbid];
    [self execSQL :sql];
}

@end
