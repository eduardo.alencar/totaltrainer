//
//  PhysicalPlan.h
//  TotalTrainer
//
//  Created by Eduardo on 08/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Global.h"
#import "S4M_PhysicalPlan.h"

@interface PhysicalPlan : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(PhysicalPlan *)physicalPlanCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(S4M_PhysicalPlan *)physicalplan;
-(void)removerItem:(S4M_PhysicalPlan *)physicalplan;
-(BOOL)salvarMudancas;
-(S4M_PhysicalPlan *)criarPhysicalPlan:(NSString *)date setHeight:(NSNumber *)cm setBF:(NSNumber *)BF setWeight:(NSNumber *)kg setObj:(NSString *)info;
-(S4M_PhysicalPlan *)upgradePhysicalPlan:(int)pos setDate:(NSString *)date setHeight:(NSNumber *)cm setBF:(NSNumber *)BF setWeight:(NSNumber *)kg setObj:(NSString *)info;
-(void)excluirPhysicalPlan:(S4M_PhysicalPlan *)physicalplan;
-(void)carregarPhysicalPlan;

@end

