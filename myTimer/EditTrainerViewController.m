//
//  EditTrainerViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 30/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "EditTrainerViewController.h"

@interface EditTrainerViewController ()

@end

@implementation EditTrainerViewController

@synthesize texer, tgrupo, tpeso, trepeticao, tserie;
@synthesize ltitle, lgroup, lexer, lserie, lrepeticao, lpeso, lhelp1, lhelp2;
@synthesize bcalcule;

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view
    alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Loading",@"") delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    
    sexer = nil;
    sgrupo = nil;
    srepeticao = nil;
    speso = nil;
    stemp = nil;
    sql = nil;
  
    UIGraphicsBeginImageContext(self.view.frame.size);
    if (IS_IPHONE_5) [[UIImage imageNamed:@"fundo-massa-corporal-02.png"] drawInRect:self.view.bounds];
    else [[UIImage imageNamed:@"fundo-massa-corporal-01.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];

    [ltitle setFont:[UIFont fontWithName:@"DINEngschriftStd" size:26]];
    [bcalcule setFont:[UIFont fontWithName:@"Exo-Medium" size:16]];
    
    [bcalcule setTitle:NSLocalizedString(@"exec_save", @"") forState:UIControlStateNormal];
    [ltitle setText:NSLocalizedString(@"exec_title","")];
    [lgroup setText:NSLocalizedString(@"exec_group","")];
    [lexer setText:NSLocalizedString(@"exec_exer","")];
    [lserie setText:NSLocalizedString(@"exec_serie","")];
    [lrepeticao setText:NSLocalizedString(@"exec_reps","")];
    [lpeso setText:NSLocalizedString(@"exec_weight","")];
    [lhelp1 setText:NSLocalizedString(@"exec_help1","")];
    [lhelp2  setText:NSLocalizedString(@"exec_help2","")];
    
    [tgrupo setDelegate:self];
    [texer setDelegate:self];
    [tserie setDelegate:self];
    [trepeticao setDelegate:self];
    [tpeso setDelegate:self];
    
    //if (DEBUG_LOG) NSLog(@"-> TREINO %@", treinogeral);
    //if ([treinogeral.tid length] > 0) {
    if (exerc_detalhe != nil) {
        [texer setText:[exerc_detalhe exercicio]];
        [tgrupo setText:[exerc_detalhe grupo]];
        [trepeticao setText:[exerc_detalhe repeticao]];
        [tpeso setText:[exerc_detalhe carga]];
    }
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)savedTrainer:(id) sender {
    sexer = [texer text];
    sgrupo = [[tgrupo text] uppercaseString];
    speso = [tpeso text];
    sserie = [tserie text];
    srepeticao = [trepeticao text];
    if ([sexer length] > 1 && [sgrupo length] > 0 && [srepeticao length] > 0) {
        if ([sgrupo length] == 2) {
            sgrupo = [NSString stringWithFormat:@"%@0%@",[sgrupo substringWithRange:NSMakeRange(0, 1)], [sgrupo substringWithRange:NSMakeRange(1, 1)]];
        }
        iserie = 0;
        @try {
            iserie = [sserie integerValue];
        }@catch(NSException *e) {
            iserie = 0;
        }
        NSLog(@" *********************** SERIES -> %i", iserie);
        if (exerc_detalhe == nil) {
            NSRange range = [srepeticao rangeOfString:@"/"];
            stemp = srepeticao;
            NSLog(@" *********************** REPETICAO FORA -> %@", srepeticao);
            if (range.location == NSNotFound) {
                for (int i = 1; i <iserie; i++) {
                    NSLog(@" ************************ REPETICAO -> %@", srepeticao);
                    srepeticao = [NSString stringWithFormat:@"%@/%@", srepeticao, stemp];
                }
                [[Exercise exerciseCompartilhado] criarExercise:sgrupo setExercise:sexer setRepeat:srepeticao setWeight:speso];
                //sql = [NSString stringWithFormat:@"insert INTO s4m_exercise(grupo, exercicio, repeticao, carga) values ('%@','%@','%@','%@');",sgrupo, sexer, srepeticao, speso];
                ////[banco execSQL:sql];
                //if (DEBUG_LOG) NSLog(@"SQL -> [%@]", sql);
            } else {
                [[Exercise exerciseCompartilhado] criarExercise:sgrupo setExercise:sexer setRepeat:srepeticao setWeight:speso];
                //sql = [NSString stringWithFormat:@"insert INTO s4m_exercise(grupo, exercicio, repeticao, carga) values ('%@','%@','%@','%@');",sgrupo, sexer, srepeticao, speso];
                ////[banco execSQL:sql];
                //if (DEBUG_LOG) NSLog(@"SQL -> [%@]", sql);
            }
        } else {
            NSRange range = [srepeticao rangeOfString:@"/"];
            stemp = srepeticao;
            if (range.location == NSNotFound) {
                for (int i = 1; i <iserie; i++) {
                    NSLog(@" ************************ REPETICAO -> %@", srepeticao);
                    srepeticao = [NSString stringWithFormat:@"%@/%@", srepeticao, stemp];
                }
                // UPDATE MIGRAR PARA COREDATA
                [[Exercise exerciseCompartilhado] upgradeExercise:treinogeral setGroup:sgrupo setExercise:sexer setRepeat:srepeticao setWeight:speso];
            } else {
                // UPDATE MIGRAR PARA COREDATA
                [[Exercise exerciseCompartilhado] upgradeExercise:treinogeral setGroup:sgrupo setExercise:sexer setRepeat:srepeticao setWeight:speso];
            }
            /*
            sql = [NSString stringWithFormat:@"update s4m_exercise set grupo='%@' where id = %@;",sgrupo, treino_id];
            /// [banco execSQL:sql];
            if (DEBUG_LOG) NSLog(@"SQL -> [%@]", sql);
            sql = [NSString stringWithFormat:@"update s4m_exercise set exercicio='%@' where id = %@;",sexer, treino_id];
            /// [banco execSQL:sql];
            if (DEBUG_LOG) NSLog(@"SQL -> [%@]", sql);
            sql = [NSString stringWithFormat:@"update s4m_exercise set repeticao='%@' where id = %@;",srepeticao, treino_id];
            /// [banco execSQL:sql];
            if (DEBUG_LOG) NSLog(@"SQL -> [%@]", sql);
            sql = [NSString stringWithFormat:@"update s4m_exercise set carga='%@' where id = %@;",speso, treino_id];
            /// [banco execSQL:sql];
            if (DEBUG_LOG) NSLog(@"SQL -> [%@]", sql);
             */
        }
        
       //back
        [self dismissViewControllerAnimated:YES completion:nil];
        //[self.navigationController popViewControllerAnimated:YES];
        //[self.view removeFromSuperview];
    } else {
        if ([sexer length] < 2) {
            
        }else if ([sgrupo length]< 1) {
            
        } else {
            
        }
    }
    
}

-(IBAction)cancelTrainer:(id)sender {
    //[self.navigationController popViewControllerAnimated:YES];A
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.view removeFromSuperview];
}

-(IBAction)editGroup:(id) sender {
    
    /*
    av_grupo = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"exec_group2", @"") message:@"Group" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"")  otherButtonTitles:@"OK", nil];
    textAlertGroup = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [textAlertGroup setBackgroundColor:[UIColor whiteColor]];
    [textAlertGroup setText:treinogeral.grupo];
    [textAlertGroup setTextAlignment:UITextAlignmentCenter];
    [av_grupo addSubview:textAlertGroup];
    [av_grupo show];
     */
}

-(IBAction)editSeries:(id) sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    self.view.frame = CGRectMake(0,-60, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];

    /*
    av_serie = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"exec_sets2", @"") message:@"Sets" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"")  otherButtonTitles:@"OK", nil];
    textAlertSerie = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [textAlertSerie setBackgroundColor:[UIColor whiteColor]];
    [textAlertSerie setTextAlignment:UITextAlignmentCenter];
    [av_serie addSubview:textAlertSerie];
    [av_serie show];
     */
}

-(IBAction)editExercise:(id) sender {
    /*
    av_exer = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"exec_exercise2", @"") message:@"Exercise" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"")  otherButtonTitles:@"OK", nil];
    //av_exer.alertViewStyle = UIAlertViewStylePlainTextInput;
    textAlertExercise = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [textAlertExercise setBackgroundColor:[UIColor whiteColor]];
    [textAlertExercise setTextAlignment:UITextAlignmentCenter];
    [textAlertExercise setText:treinogeral.exercicio];
    [av_exer addSubview:textAlertExercise];
    [av_exer show];
     */
}

-(IBAction)editRepeat:(id) sender{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    self.view.frame = CGRectMake(0,-100, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];

    /*
    av_repeticao = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"exec_reps2", @"") message:@"Reps" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:@"OK", nil];
    textAlertRepeat = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [textAlertRepeat setBackgroundColor:[UIColor whiteColor]];
    [textAlertRepeat setTextAlignment:UITextAlignmentCenter];
    [textAlertRepeat setText:treinogeral.repeticao];
    [av_repeticao addSubview:textAlertRepeat];
    [av_repeticao show];
     */
}

-(IBAction)editWeight:(id) sender{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    self.view.frame = CGRectMake(0,-140, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];

    /*
    av_peso = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"exec_weight2", @"") message:@"Weight" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:@"OK", nil];
    av_peso.alertViewStyle = UIAlertViewStylePlainTextInput;
    textAlertweight = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
    [textAlertweight setBackgroundColor:[UIColor whiteColor]];
    [textAlertweight setTextAlignment:UITextAlignmentCenter];
    [textAlertweight setText:[tpeso text]];
    [av_peso addSubview:textAlertweight];
    [av_peso show];
     */
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    if (theTextField == tgrupo) {
        [texer becomeFirstResponder];
    } else if (theTextField == texer) {
        [theTextField becomeFirstResponder];
        [tserie becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-60, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else if (theTextField == tserie) {
        [trepeticao becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-100, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else if (theTextField == trepeticao) {
        [tpeso becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-140, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        
    }
    return YES;
}


/*
-(void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet == av_exer) {
        [texer setText:[textAlertExercise text]];
    }else if (actionSheet == av_grupo) {
        [tgrupo setText:[textAlertGroup text]];
    }else if (actionSheet == av_peso) {
        [tpeso setText:[textAlertweight text]];
    }else if (actionSheet == av_repeticao) {
        [trepeticao setText:[textAlertRepeat text]];
    }else {
        [tserie setText:[textAlertSerie text]];
    }
}
 */

@end
