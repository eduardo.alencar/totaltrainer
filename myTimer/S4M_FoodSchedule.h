//
//  S4M_FoodSchedule.h
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_FoodSchedule : NSManagedObject

@property (nonatomic, retain) NSDate * date_begin;
@property (nonatomic, retain) NSDate * date_end;
@property (nonatomic, retain) NSNumber * foodplan_id;
@property (nonatomic, retain) NSNumber * id;

@end
