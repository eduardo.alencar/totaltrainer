//
//  FoodPlanTableViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 11/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodPlanTableViewController  : UIViewController  <UITableViewDelegate, UITableViewDataSource> {
    UITableViewCell *tableViewCelula;
    UITableView *tableFood;
    UILabel *lday_hour, *lnome, *ltype, *ltitle;
    UIAlertView *alert;
    NSMutableArray *lista;
    //TrainerTableViewController *trainerController;
    //IBOutlet myTimerViewController *myTimerController;
}

@property(nonatomic, retain) IBOutlet UITableViewCell *tableViewCelula;
@property(nonatomic, retain) IBOutlet UITableView *tableFood;
@property(nonatomic, retain) IBOutlet UILabel *lday_hour, *lnome, *ltype, *ltitle;
//@property(nonatomic, retain) TrainerTableViewController *trainerController;

-(void)getInfoFoodPlan;
-(void)reloadTableView;
-(IBAction)sendBack:(id) sender;
-(IBAction)sendAddFoodPlan:(id) sender;

@end
