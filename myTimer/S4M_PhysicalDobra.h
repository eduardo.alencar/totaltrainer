//
//  S4M_PhysicalDobra.h
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_PhysicalDobra : NSManagedObject

@property (nonatomic, retain) NSNumber * abdominal;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * panturrilha;
@property (nonatomic, retain) NSNumber * peitoral;
@property (nonatomic, retain) NSNumber * physicalplan_id;
@property (nonatomic, retain) NSNumber * quadriceps;
@property (nonatomic, retain) NSNumber * subescapular;
@property (nonatomic, retain) NSNumber * suprailiaca;
@property (nonatomic, retain) NSNumber * triciptal;

@end
