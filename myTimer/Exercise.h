//
//  Exercise.h
//  TotalTrainer
//
//  Created by Eduardo on 31/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "S4M_Exercise.h"
#import "Trainer.h"
#import "Global.h"

@interface Exercise : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(Exercise *)exerciseCompartilhado;
-(NSArray *)itens;
-(int)MaxTrainer;
-(void)adicionarItem:(S4M_Exercise *)exercise;
-(void)removerItem:(S4M_Exercise *)exercise;
-(BOOL)salvarMudancas;
-(S4M_Exercise *)criarExercise:(NSString *)grupo setExercise:(NSString *)exercicio setRepeat:(NSString *)repeticao setWeight:(NSString *)carga;
-(S4M_Exercise *)upgradeExercise:(int) pos setGroup:(NSString *)grupo setExercise:(NSString *)exercicio setRepeat:(NSString *)repeticao setWeight:(NSString *)carga;

-(void)excluirExercicio:(S4M_Exercise *)exerc;
-(void)carregarExercise;
-(NSMutableArray *)carregarExercise:(NSString *)day;
-(void)migrarExercise :(NSMutableArray *)Info;

@end
