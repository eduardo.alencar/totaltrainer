//
//  WorkoutTableViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 09/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "WorkoutTableViewController.h"

@interface WorkoutTableViewController ()

@end

@implementation WorkoutTableViewController

@synthesize lobj;
@synthesize lnome;
@synthesize ldata;
@synthesize tableTreino;
@synthesize tableViewCelula;

@synthesize trainerController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (DEBUG_LOG) NSLog(@"TAMANHO DA TABELA DE PROJETOS -> [%i]", [lista count]);
    return 0;
}

-(void)getInfoWorkout {
    
}

-(void)reloadTableView {
    
}

-(IBAction)sendBack:(id) sender {
    
}

-(IBAction)sendAddWorkout:(id) sender {
    
}

@end
