//
//  Food.h
//  TotalTrainer
//
//  Created by Eduardo on 05/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Global.h"
#import "S4M_Foods.h"

@interface Food : NSObject {
    NSMutableArray *itens;
    NSManagedObjectContext *contexto;
    NSManagedObjectModel *modelo;
    NSPersistentStoreCoordinator *psc;
    NSURL *urlArmazenamento;
}

+(Food *)foodCompartilhado;
-(NSArray *)itens;
-(void)adicionarItem:(S4M_Foods *)food;
-(void)removerItem:(S4M_Foods *)food;
-(BOOL)salvarMudancas;
-(S4M_Foods *)criarFood:(NSString *)type setCategory:(NSString *)categoria setName:(NSString *)name setCode:(NSString *)code setCarbohydrates:(NSString *)carbohydrates setCalories:(NSString *)calories setProteins:(NSString *)proteins setSodio:(NSString *)sodio;
-(S4M_Foods *)upgradeFood:(int)pos setType:(NSString *)type setCategory:(NSString *)categoria setName:(NSString *)name setCode:(NSString *)code setCarbohydrates:(NSString *)carbohydrates setCalories:(NSString *)calories setProteins:(NSString *)proteins setSodio:(NSString *)sodio;
-(void)excluirFoods:(S4M_Foods *)food;
-(void)carregarFoods;
-(NSMutableArray *)carregarFoods:(NSString *)type;


@end
