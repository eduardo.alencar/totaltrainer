//
//  FoodSchedule.m
//  TotalTrainer
//
//  Created by Eduardo on 06/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "FoodSchedule.h"

@implementation FoodSchedule

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSError *erro = nil;
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarFoodschedule];
    }
    return self;
}

+(FoodSchedule *)foodScheduleCompartilhado {
    static FoodSchedule *foodscheduleCompartilhado = nil;
    if (!foodscheduleCompartilhado) foodscheduleCompartilhado = [[super allocWithZone:nil] init];
    return foodscheduleCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self foodScheduleCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_FoodSchedule *)foodschedule{
    [itens addObject:foodschedule];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_FoodSchedule *)foodschedule {
    [contexto deleteObject:foodschedule];
    [itens removeObject:foodschedule];
    [self salvarMudancas];
}

-(void)carregarFoodschedule {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_FoodSchedule"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"plan_id" ascending:YES];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta FoodSchedule" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(NSMutableArray *)carregarFoodschedule:(NSString *)plan_id {
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_FoodSchedule"];
    [requisicao setEntity:entidade];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"plan_id CONTAINS %@ ",plan_id];
    [requisicao setPredicate:predicate];
    NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"plan_id" ascending:YES];
    [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta FoodSchedule" format:@"Motivo: %@", [erro localizedDescription]];
    NSMutableArray *listtotal =  [resultado copy];
    NSMutableArray *listaG = [[NSMutableArray alloc] init];
    for (int i = 0; [listtotal count] > i; i++) {
        
    }
    NSLog(@"Lista de FOODSCHEDULE -> %@",listaG );
    return listaG;
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarFoodschedule];
    return sucess;
}

-(S4M_FoodSchedule *)criarFoodschedule:(NSString *)plan_id setDateBegin:(NSString *)datebegin setDateEnd:(NSString *)dateend {
    S4M_FoodSchedule *food = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_FoodSchedule" inManagedObjectContext:contexto];
    [food setFoodplan_id:[NSNumber numberWithInteger: [plan_id integerValue]]];
    
    datebegin = [NSString stringWithFormat:@"%@%@%@ %@:%@",[datebegin substringWithRange:NSMakeRange(6,4)],[datebegin substringWithRange:NSMakeRange(3,2)],[datebegin substringWithRange:NSMakeRange(0,2)], [datebegin substringWithRange:NSMakeRange(11,2)], [datebegin substringWithRange:NSMakeRange(14,2)]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd hh:mm"];
    NSDate *datebegin2 = [dateFormat dateFromString:datebegin];
    [food setDate_begin:datebegin2];
    
    dateend = [NSString stringWithFormat:@"%@%@%@",[dateend substringWithRange:NSMakeRange(6,4)],[dateend substringWithRange:NSMakeRange(3,2)],[dateend substringWithRange:NSMakeRange(0,2)]];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSDate *dateend2 = [dateFormat dateFromString:dateend];
    [food setDate_end:dateend2];
    [itens addObject:food];
    
    [self salvarMudancas];
    [self carregarFoodschedule];
    return food;
}

-(S4M_FoodSchedule *)upgradeFoodschedule:(int)pos setPlan:(NSString *)plan_id setDateBegin:(NSString *)datebegin setDateEnd:(NSString *)dateend {
    [self carregarFoodschedule];
    S4M_FoodSchedule *food = [itens objectAtIndex:pos];
    [food setFoodplan_id:[NSNumber numberWithInteger: [plan_id integerValue]]];
    
    datebegin = [NSString stringWithFormat:@"%@%@%@ %@:%@",[datebegin substringWithRange:NSMakeRange(6,4)],[datebegin substringWithRange:NSMakeRange(3,2)],[datebegin substringWithRange:NSMakeRange(0,2)], [datebegin substringWithRange:NSMakeRange(11,2)], [datebegin substringWithRange:NSMakeRange(14,2)]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd hh:mm"];
    NSDate *datebegin2 = [dateFormat dateFromString:datebegin];
    [food setDate_begin:datebegin2];
    dateend = [NSString stringWithFormat:@"%@%@%@",[dateend substringWithRange:NSMakeRange(6,4)],[dateend substringWithRange:NSMakeRange(3,2)],[dateend substringWithRange:NSMakeRange(0,2)]];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSDate *dateend2 = [dateFormat dateFromString:dateend];
    [food setDate_end:dateend2];
    
    [self salvarMudancas];
    [self carregarFoodschedule];
    return food;
}

-(void)excluirFoodSchedule:(S4M_FoodSchedule *)foodschedule {
    [contexto deleteObject:foodschedule];
    [itens removeObjectIdenticalTo:foodschedule];
}

@end
