//
//  Setup.m
//  TotalTrainer
//
//  Created by Eduardo on 31/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "Setup.h"

@implementation Setup

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        //urlArmazenamento = [[ubiq URLByAppendingPathComponent:@"Documents"] URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSLog(@"[Setup] DIRETORIO PARA iCLOUD -> %@", urlArmazenamento);
        NSError *erro = nil;
        
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:nil error:&erro];
        //if (![psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:nil error:&erro])[NSException raise:@"Falha na abertura da base" format:@"ERROR 002 : %@",[erro localizedDescription]];
        
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarSetup];
    }
    return self;
}

+(Setup *)setupCompartilhado {
    static Setup *setupCompartilhado = nil;
    if (!setupCompartilhado) setupCompartilhado = [[super allocWithZone:nil] init];
    return setupCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self setupCompartilhado];
}

-(NSMutableArray *)itens {
    return itens;
}

-(void)adicionarItem:(NSString *)setup{
    [itens addObject:setup];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)carregarSetup {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Setup"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta Setup" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}



-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarSetup];
    return sucess;
}

-(S4M_Setup *)criarSetup:(NSString *)pass setLogin:(NSString *)login setFacebook:(NSNumber *)facebook setGPS:(NSNumber *) gps setTotalHour:(NSNumber *) total_hour setTotalMin:(NSNumber *)total_min setIntervalMin:(NSNumber *)interval_min setIntervalSec:(NSNumber *) interval_sec setUnit:(NSString *) unit setGrupo:(NSString *) grupo setSound:(NSNumber *) sound setSpeak:(NSNumber *) speak  {
    S4M_Setup *setup = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Setup" inManagedObjectContext:contexto];
    [itens removeAllObjects];
    [setup setFacebook:facebook];
    [setup setLogin:login];
    [setup setPass:pass];
    [setup setGps:gps];
    [setup setTotal_hour:total_hour];
    [setup setTotal_min:total_min];
    [setup setInterval_min:interval_min];
    [setup setInterval_sec:interval_sec];
    [setup setUnit:unit];
    [setup setGrupo:grupo];
    [setup setSound:sound];
    [setup setSpeak:speak];
    
    [itens addObject:setup];
    
    [self salvarMudancas];
    [self carregarSetup];
    return setup;
}

-(S4M_Setup *)upgradeSetup:(NSString *)pass setLogin:(NSString *)login setFacebook:(NSNumber *)facebook setGPS:(NSNumber *) gps setTotalHour:(NSNumber *) total_hour setTotalMin:(NSNumber *)total_min setIntervalMin:(NSNumber *)interval_min setIntervalSec:(NSNumber *) interval_sec setUnit:(NSString *) unit setGrupo:(NSString *) grupo setSound:(NSNumber *) sound setSpeak:(NSNumber *) speak{
    [self carregarSetup];
    S4M_Setup *setup;
    if ([itens count] > 0) {
        setup = [itens objectAtIndex:0];
        [setup setFacebook:facebook];
        [setup setLogin:login];
        [setup setPass:pass];
        [setup setGps:gps];
        [setup setTotal_hour:total_hour];
        [setup setTotal_min:total_min];
        [setup setInterval_min:interval_min];
        [setup setInterval_sec:interval_sec];
        [setup setUnit:unit];
        [setup setGrupo:grupo];
        [setup setSound:sound];
        [setup setSpeak:speak];
    } else {
        setup = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Setup" inManagedObjectContext:contexto];
        [itens removeAllObjects];
        [setup setFacebook:facebook];
        [setup setLogin:login];
        [setup setPass:pass];
        [setup setGps:gps];
        [setup setTotal_hour:total_hour];
        [setup setTotal_min:total_min];
        [setup setInterval_min:interval_min];
        [setup setInterval_sec:interval_sec];
        [setup setUnit:unit];
        [setup setGrupo:grupo];
        [setup setSound:sound];
        [setup setSpeak:speak];
        
        [itens addObject:setup];
    }
    [self salvarMudancas];
    [self carregarSetup];
    return setup;
}

//MIGRAÇÃO
-(void)migrarSetup :(NSMutableArray *)Info {
    NSLog(@"dados para migraçao recebidos [%i] -> %@", [Info count],Info);
    if  ([Info count] == 11) {
        S4M_Setup *sp = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Setup" inManagedObjectContext:contexto];
        [sp setId:[NSNumber numberWithInt:1]];
        [sp setFacebook:([[Info objectAtIndex:0] isEqual: @"YES"]? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0])];
        [sp setLogin:[Info objectAtIndex:1]];
        [sp setPass:[Info objectAtIndex:2]];
        [sp setGps:([[Info objectAtIndex:3] isEqual: @"YES"]? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0])];
        [sp setTotal_hour:[NSNumber numberWithInt:[Info objectAtIndex:4]]];
        [sp setTotal_min:[NSNumber numberWithInt:[Info objectAtIndex:5]]];
        [sp setInterval_min:[NSNumber numberWithInt:[Info objectAtIndex:6]]];
        [sp setInterval_sec:[NSNumber numberWithInt:[Info objectAtIndex:7]]];
        [sp setUnit:@""];
        [sp setGrupo:[Info objectAtIndex:8]];
        [sp setSound:([[Info objectAtIndex:9] isEqual: @"YES"]? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0])];
        [sp setSpeak:([[Info objectAtIndex:10] isEqual: @"YES"]? [NSNumber numberWithInt:1] : [NSNumber numberWithInt:0])];
        [contexto deletedObjects];
        [itens removeAllObjects];
        [self salvarMudancas];
        [itens addObject:sp];
        [self salvarMudancas];
        [self carregarSetup];
    }
}

-(NSMutableArray *)getIntervaloGeral {
    NSMutableArray *list = [[NSMutableArray alloc] init];
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Setup"];
    [requisicao setEntity:entidade];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta Setup" format:@"Motivo: %@", [erro localizedDescription]];
    if ([resultado count] > 0) {
        [list addObject:[[resultado objectAtIndex:0] interval_min]];
        [list addObject:[[resultado objectAtIndex:0] interval_sec]];
        NSLog(@"INTERVALO ENCONTRADO -> %@", list);
    } else {
        [list addObject:@"0"];
        [list addObject:@"45"];
    }
    return list;
}

@end
