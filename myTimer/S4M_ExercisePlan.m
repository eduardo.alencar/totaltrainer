//
//  S4M_ExercisePlan.m
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "S4M_ExercisePlan.h"


@implementation S4M_ExercisePlan

@dynamic date_begin;
@dynamic date_end;
@dynamic id;
@dynamic name;
@dynamic hour;
@dynamic objective;

@end
