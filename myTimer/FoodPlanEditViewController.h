//
//  FoodPlanEditViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 11/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodPlanEditViewController : UIViewController<UITextFieldDelegate> {
    IBOutlet UITextField *tname, *tday_week, *thour, *tqtd, *ttype;
    IBOutlet UILabel *ltitle, *lname, *lday_week, *lhour, *lqtd, *ltype, *lalarm;
    IBOutlet UIButton *bsave, *bfood, *bschedule;
    NSString *food_id;
}

@property(nonatomic, retain) IBOutlet UITextField*tname, *tday_week, *thour, *tqtd, *ttype;
@property(nonatomic, retain) IBOutlet UILabel *ltitle, *lname, *lday_week, *lhour, *lqtd, *ltype, *lalarm;
@property(nonatomic, retain) IBOutlet UIButton *bsave, *bfood, *bschedule;

-(IBAction)savedFoodPlan:(id) sender;
-(IBAction)editFoodPlan:(id) sender;

@end
