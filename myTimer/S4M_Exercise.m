//
//  S4M_Exercise.m
//  TotalTrainer
//
//  Created by Eduardo on 29/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "S4M_Exercise.h"


@implementation S4M_Exercise

@dynamic id;
@dynamic grupo;
@dynamic exercicio;
@dynamic repeticao;
@dynamic carga;
@dynamic pos;
@dynamic intervalo;
@dynamic conjugado;
@dynamic plan_id;

@end
