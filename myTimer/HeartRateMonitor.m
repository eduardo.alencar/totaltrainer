//
//  HeartRateMonitor.m
//  TotalTrainer
//
//  Created by Eduardo on 06/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "HeartRateMonitor.h"

@implementation HeartRateMonitor

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSError *erro = nil;
         NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
 
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarHeartRateMonitor];
    }
    return self;
}

+(HeartRateMonitor *)exerciseCompartilhado {
    static HeartRateMonitor *heartCompartilhado = nil;
    if (!heartCompartilhado) heartCompartilhado = [[super allocWithZone:nil] init];
    return heartCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self exerciseCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_HeartRateMonitor *)heart{
    [itens addObject:heart];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_HeartRateMonitor *)heart {
    [contexto deleteObject:heart];
    [itens removeObject:heart];
    [self salvarMudancas];
}

-(void)carregarHeartRateMonitor {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_HeartRateMonitor"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"data" ascending:NO];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta HeartRateMonitor" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(NSMutableArray *)carregarHeartRateMonitor:(NSString *)day{
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_HeartRateMonitor"];
    [requisicao setEntity:entidade];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"data CONTAINS %@ ",day];
    [requisicao setPredicate:predicate];
    NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"data" ascending:NO];
    [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta HeartRateMonitor" format:@"Motivo: %@", [erro localizedDescription]];
    NSMutableArray *listtotal =  [resultado copy];
    NSMutableArray *listaG = [[NSMutableArray alloc] init];
    for (int i = 0; [listtotal count] > i; i++) {
   }
    return listaG;
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarHeartRateMonitor];
    return sucess;
}

-(S4M_HeartRateMonitor *)criarHeartRateMonitor:(NSString *)data setBPM:(NSString *)bpm {
    S4M_HeartRateMonitor *heart = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_HeartRateMonitor" inManagedObjectContext:contexto];
    
    data = [NSString stringWithFormat:@"%@%@%@",[data substringWithRange:NSMakeRange(6,4)],[data substringWithRange:NSMakeRange(3,2)],[data substringWithRange:NSMakeRange(0,2)]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSDate *data2 = [dateFormat dateFromString:data];
    [heart setData:data2];
    [heart setBpm:[NSNumber numberWithInteger: [bpm integerValue]]];
    [itens addObject:heart];
    [self salvarMudancas];
    [self carregarHeartRateMonitor];
    return heart;
}

-(S4M_HeartRateMonitor *)upgradeExercise:(int)pos setData:(NSString *)data setBPM:(NSString *)bpm{
    [self carregarHeartRateMonitor];
    S4M_HeartRateMonitor *heart = [itens objectAtIndex:pos];
    data = [NSString stringWithFormat:@"%@%@%@",[data substringWithRange:NSMakeRange(6,4)],[data substringWithRange:NSMakeRange(3,2)],[data substringWithRange:NSMakeRange(0,2)]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSDate *data2 = [dateFormat dateFromString:data];
    [heart setData:data2];
    [heart setBpm:[NSNumber numberWithInteger: [bpm integerValue]]];
    
    [self salvarMudancas];
    [self carregarHeartRateMonitor];
    return heart;
}

-(void)excluirHeartRateMonitor:(S4M_HeartRateMonitor *)heart {
    [contexto deleteObject:heart];
    [itens removeObjectIdenticalTo:heart];
}

@end
