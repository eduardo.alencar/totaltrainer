//
//  FoodEditViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 11/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodEditViewController : UIViewController<UITextFieldDelegate> {
    IBOutlet UITextField *tproteina, *tbarcode, *tname, *tcategory, *ttype, *tcarboidrato, *tsodio, *tcalorias;
    IBOutlet UILabel *ltitle, *lname, *lcategory, *ltype, *lbarcode, *lproteina, *lcarboidrato, *lsodio, *lcalorias;
    IBOutlet UIButton *bsave;
    NSString *food_id;
}

@property(nonatomic, retain) IBOutlet UITextField*tproteina, *tbarcode, *tname, *tcategory, *ttype, *tcarboidrato, *tsodio, *tcalorias;
@property(nonatomic, retain) IBOutlet UILabel *ltitle, *lname, *lcategory, *ltype, *lbarcode, *lproteina, *lcarboidrato, *lsodio, *lcalorias;
@property(nonatomic, retain) IBOutlet UIButton *bsave;

-(IBAction)savedFood:(id) sender;

@end
