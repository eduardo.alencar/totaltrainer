//
//  S4M_FoodPlan.m
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "S4M_FoodPlan.h"


@implementation S4M_FoodPlan

@dynamic food_id;
@dynamic hour;
@dynamic id;
@dynamic qtd;
@dynamic tipo;
@dynamic day_week;

@end
