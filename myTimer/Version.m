//
//  Version.m
//  TotalTrainer
//
//  Created by Eduardo on 31/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "Version.h"

@implementation Version

- (id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        //urlArmazenamento = [[ubiq URLByAppendingPathComponent:@"Documents"] URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSLog(@"[Version] DIRETORIO PARA iCLOUD -> %@", urlArmazenamento);
        NSError *erro = nil;
        VERSION = @"2.1.0";
    
        //if (![psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:nil error:&erro])[NSException raise:@"Falha na abertura da base" format:@"ERROR 001 : %@",[erro localizedDescription]];
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:nil error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarVersion];
    }
    return self;
}

+(Version *)versionCompartilhado {
    static Version *versionCompartilhado = nil;
    if (!versionCompartilhado) versionCompartilhado = [[super allocWithZone:nil] init];
    return versionCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self versionCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(NSString *)setup{
    [itens addObject:setup];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

-(void)carregarVersion {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_Version"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"ID" ascending:YES];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta Version" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarVersion];
    return sucess;
}

-(S4M_Version *)criarVersao {
    S4M_Version *version = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Version" inManagedObjectContext:contexto];
    [itens removeAllObjects];
    [itens addObject:version];
    
    [self salvarMudancas];
    [self carregarVersion];
    return version;
}

//MIGRAÇÃO
-(void)migrarVersion {
    S4M_Version *vs = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_Version" inManagedObjectContext:contexto];
    [vs setVersion:VERSION];
    [vs setId:@"1"];
    [contexto deletedObjects];
    [itens removeAllObjects];
    [itens addObject:vs];
    [self salvarMudancas];
    [self carregarVersion];
}

@end
