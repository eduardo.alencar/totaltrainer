//
//  FoodPlan.m
//  TotalTrainer
//
//  Created by Eduardo on 08/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "FoodPlan.h"

@implementation FoodPlan

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSError *erro = nil;
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarFoodPlan];
    }
    return self;
}

+(FoodPlan *)foodPlanCompartilhado {
    static FoodPlan *foodPlanCompartilhado = nil;
    if (!foodPlanCompartilhado) foodPlanCompartilhado = [[super allocWithZone:nil] init];
    return foodPlanCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self foodPlanCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_FoodPlan *)foodplan {
    [itens addObject:foodplan];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_FoodPlan *)foodplan {
    [contexto deleteObject:foodplan];
    [itens removeObject:foodplan];
    [self salvarMudancas];
}

-(void)carregarFoodPlan {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_FoodPlan"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"food_id" ascending:YES];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta FoodPlan" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(NSMutableArray *)carregarFoodPlan:(NSString *)food_id {
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_FoodPlan"];
    [requisicao setEntity:entidade];
    NSLog(@"TYPE -> %@", food_id);
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"food_id CONTAINS %@ ",food_id];
    [requisicao setPredicate:predicate];
    NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"food_id" ascending:YES];
    [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta FoodPlan" format:@"Motivo: %@", [erro localizedDescription]];
    NSMutableArray *listtotal =  [resultado copy];
    NSMutableArray *listaG = [[NSMutableArray alloc] init];
    for (int i = 0; [listtotal count] > i; i++) {
        
    }
    return listaG;
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarFoodPlan];
    return sucess;
}

-(S4M_FoodPlan *)criarFoodPlan:(NSString *)food_id setType:(NSString *)type setQTD:(NSNumber *)qtd setDayWeek:(NSString *)dayWeek setHour:(NSString *)hour {
    S4M_FoodPlan *food = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_FoodPlan" inManagedObjectContext:contexto];
    [food setFood_id:[NSNumber numberWithInteger: [food_id integerValue]]];
    [food setTipo:type];
    [food setQtd:qtd];
    [food setDay_week:dayWeek];
    [food setHour:hour];
    [itens addObject:food];
    
    [self salvarMudancas];
    [self carregarFoodPlan];
    return food;
}

-(S4M_FoodPlan *)upgradeFoodPlan:(int)pos setFood:(NSString *)food_id setType:(NSString *)type setQTD:(NSNumber *)qtd setDayWeek:(NSString *)dayWeek setHour:(NSString *)hour {
    [self carregarFoodPlan];
    S4M_FoodPlan *food = [itens objectAtIndex:pos];
    [food setFood_id:[NSNumber numberWithInteger: [food_id integerValue]]];
    [food setTipo:type];
    [food setQtd:qtd];
    [food setDay_week:dayWeek];
    [food setHour:hour];
    
    [self salvarMudancas];
    [self carregarFoodPlan];
    return food;
}

-(void)excluirFoodPlan:(S4M_FoodPlan *)foodplan {
    [contexto deleteObject:foodplan];
    [itens removeObjectIdenticalTo:foodplan];
}

@end
