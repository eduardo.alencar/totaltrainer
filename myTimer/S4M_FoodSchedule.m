//
//  S4M_FoodSchedule.m
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "S4M_FoodSchedule.h"


@implementation S4M_FoodSchedule

@dynamic date_begin;
@dynamic date_end;
@dynamic foodplan_id;
@dynamic id;

@end
