//
//  EditWorkoutViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 10/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditWorkoutViewController : UIViewController<UITextFieldDelegate> {
    IBOutlet UITextField *tname, *tdatabegin, *tdataend, *tobj, *thour;
    IBOutlet UILabel *ltitle, *lname, *ldatabegin, *ldataend, *lobj, *lhour;
    IBOutlet UIButton *bsave, *bworkout;
}

@property(nonatomic, retain) IBOutlet UITextField *tname, *tdatabegin, *tdataend, *tobj, *thour;
@property(nonatomic, retain) IBOutlet UILabel *ltitle, *lname, *ldatabegin, *ldataend, *lobj, *lhour;
@property(nonatomic, retain) IBOutlet UIButton *bsave, *bworkout;

-(IBAction)savedWorkoutPlan:(id) sender;
-(IBAction)editWorkoutPlan:(id) sender;

@end
