//
//  S4M_Version.m
//  TotalTrainer
//
//  Created by Eduardo on 29/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "S4M_Version.h"


@implementation S4M_Version

@dynamic id;
@dynamic version;

@end
