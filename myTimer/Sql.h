//
//  Sql.h
//  CadeEncomenda
//
//  Created by LebaS on 14/01/12.
//  Copyright 2012 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "Trainer.h"
#import "Version.h"
#import "Setup.h"
#import "Exercise.h"

@interface Sql : NSObject {
	sqlite3 *database;
	NSMutableArray *todosTimes;
    NSString *documentDirectory;
    NSMutableArray *list_trainer;
}

@property(retain) NSMutableArray *todosTimes;

-(NSString *)filePath;
-(BOOL)openDB;
-(void)createDB;
-(void)upgradeVersion;
-(void)execSQL: (NSString *)tmp;
-(NSMutableArray*)getTimes;
-(NSMutableArray*)getTrainer;
-(int)MaxTrainer;
-(void)MigrarSQL;
-(NSMutableArray*)getTrainer: (NSString*) Day;
-(NSMutableArray*)getTrainerFB: (NSString*) Day;
-(void)atualizarGrupo: (int)grupo;
-(void)atualizarFB:(NSString *)fbid;
-(void)atualizarTimes: (NSMutableArray *)lTimes;

@end
