//
//  S4M_PhysicalPerime.h
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_PhysicalPerime : NSManagedObject

@property (nonatomic, retain) NSNumber * abdomen;
@property (nonatomic, retain) NSNumber * antebraco;
@property (nonatomic, retain) NSNumber * biceps;
@property (nonatomic, retain) NSNumber * cintura;
@property (nonatomic, retain) NSNumber * coxa;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * panturrilha;
@property (nonatomic, retain) NSNumber * peitoral;
@property (nonatomic, retain) NSNumber * physicalplan_id;
@property (nonatomic, retain) NSNumber * quadril;

@end
