//
//  LoginViewController.h
//  TotalTrainer
//
//  Created by Eduardo on 29/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "Perfil.h"
#import "MainViewController.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface LoginViewController : UIViewController<UITextFieldDelegate, FBLoginViewDelegate> {
    UIAlertView *alert;
    MainViewController *timerController;
    Perfil *infoPessoal;
}

@property(nonatomic, retain) UIAlertView *alert;
@property(nonatomic, retain) MainViewController *timerController;
@property(nonatomic, retain) IBOutlet UILabel *ltitile_login, *linfo1, *linfo2;
@property(nonatomic, retain) IBOutlet UIButton *bskip;

-(IBAction)SkipClick:(UIButton *)sender;
-(IBAction)S4MClick:(UIButton *)sender;

@end
