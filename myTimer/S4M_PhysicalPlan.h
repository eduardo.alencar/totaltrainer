//
//  S4M_PhysicalPlan.h
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_PhysicalPlan : NSManagedObject

@property (nonatomic, retain) NSNumber * altura;
@property (nonatomic, retain) NSNumber * bf;
@property (nonatomic, retain) NSDate * data;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * objetivo;
@property (nonatomic, retain) NSNumber * peso;

@end
