//
//  TableMenuViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 30/01/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "TableMenuViewController.h"

@interface TableMenuViewController ()

@end

@implementation TableMenuViewController

@synthesize viewMain;

#pragma mark -
#pragma mark View Did Load/Unload

- (void)viewDidLoad{
    [super viewDidLoad];
    [self setupAnimalsArray];
}

- (void)viewDidUnload{
    [super viewDidUnload];
}

#pragma mark -
#pragma mark View Will/Did Appear

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
}

#pragma mark -
#pragma mark View Will/Did Disappear

- (void)viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
}

#pragma mark -
#pragma mark Array Setup

- (void)setupAnimalsArray{
    NSArray *opcoes = @[[NSArray arrayWithObjects:@"Perfil", [UIImage imageNamed:@"icone29.png"], nil],
                        [NSArray arrayWithObjects:@"Trainer", [UIImage imageNamed:@"icone29.png"], nil],
                        [NSArray arrayWithObjects:@"Workouts", [UIImage imageNamed:@"icone29.png"], nil],
                        [NSArray arrayWithObjects:@"Evolution", [UIImage imageNamed:@"icone29.png"], nil],
                        [NSArray arrayWithObjects:@"Diet", [UIImage imageNamed:@"icone29.png"], nil],
                        [NSArray arrayWithObjects:@"Schedule", [UIImage imageNamed:@"icone29.png"], nil],
                        [NSArray arrayWithObjects:@"Report", [UIImage imageNamed:@"icone29.png"], nil],
                        [NSArray arrayWithObjects:@"Setup", [UIImage imageNamed:@"icone29.png"], nil]];
                          
    self.arrayOfAnimals = [NSMutableArray arrayWithArray:opcoes];
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark UITableView Datasource/Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrayOfAnimals count];
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}
*/

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    _cellMain = [tableView dequeueReusableCellWithIdentifier:@"table_line_title"];
    //if (_cellMain == nil) [[NSBundle mainBundle] loadNibNamed:@"MainCellLeft" owner:self options:nil];
    UIImageView *mainImage = (UIImageView *)[_cellMain viewWithTag:5011];
    UILabel *imageTitle = (UILabel *)[_cellMain viewWithTag:5012];
    if ([_arrayOfAnimals count] > 0){
        NSArray *it = [self.arrayOfAnimals objectAtIndex:indexPath.row];
        NSLog(@"TEXTO -> %@", it);
        mainImage.image = [it objectAtIndex:1];
        imageTitle.text = [it objectAtIndex:0];
    }
    return _cellMain;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //Animal *currentRecord = [self.arrayOfAnimals objectAtIndex:indexPath.row];
    
    // Return Data to delegate: either way is fine, although passing back the object may be more efficient
    // [_delegate imageSelected:currentRecord.image withTitle:currentRecord.title withCreator:currentRecord.creator];
    // [_delegate animalSelected:currentRecord];
    
    //[_delegate animalSelected:currentRecord];
    NSLog(@"LINHA -> %ld", (long)indexPath.row);
    int line = indexPath.row;
    if (line == 0) {
        //perfil
        
    } else if (line == 1) {
        //trainer
        
    } else if (line == 2) {
        //workouts
        
    } else if (line == 3) {
        //evolution
        BodyFatViewController *bfcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"bodyfat_view"];;
        [viewMain addSubview:bfcontroller.view];
    } else if (line == 4) {
        // diet
        
    } else if (line == 5) {
        // schedule
        
    } else if (line == 6) {
        //report
        
    } else if (line == 7) {
        //setup
        
    } else {}
}

#pragma mark -
#pragma mark Default System Code
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

-(void)setMainView:(UIView *)newView {
    viewMain = newView;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showProfile"]) {
        //NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        //RecipeDetailViewController *destViewController = segue.destinationViewController;
        //destViewController.recipeName = [recipes objectAtIndex:indexPath.row];
        NSLog(@"Chamada para Profile ... ");
        BodyFatViewController *bfcontroller = segue.destinationViewController;
        [viewFront addSubview:bfcontroller.view];
    } else {
        NSLog(@"Chamada DIFERENTE. ");
    }
}
@end
