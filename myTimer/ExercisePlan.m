//
//  ExercisePlan.m
//  TotalTrainer
//
//  Created by Eduardo on 04/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "ExercisePlan.h"

@implementation ExercisePlan

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSLog(@"[ExercisePlan] DIRETORIO PARA iCLOUD -> %@", urlArmazenamento);
        NSError *erro = nil;
        
        ///if (![psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:nil error:&erro]) {
        ///[NSException raise:@"Falha na abertura da base" format:@"ERROR 003 : %@",[erro localizedDescription]];
        //urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        //NSError *error = nil;
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        //psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        
        /*
         if (![psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro]) {
         // Handle error
         NSLog(@"ERROR 0066 - ERRO NA TENTATIVA DUPLA DO EXERCISE");
         [NSException raise:@"Falha na abertura da base" format:@"ERROR 003 : %@",[erro localizedDescription]];
         }
         */
        //}
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarExercisePlan];
    }
    return self;
}

+(ExercisePlan *)exercisePlanCompartilhado {
    static ExercisePlan *exercisePlanCompartilhado = nil;
    if (!exercisePlanCompartilhado) exercisePlanCompartilhado = [[super allocWithZone:nil] init];
    return exercisePlanCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self exercisePlanCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_ExercisePlan *)exerciseplan{
    [itens addObject:exerciseplan];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_ExercisePlan *)exerciseplan {
    [contexto deleteObject:exerciseplan];
    [itens removeObject:exerciseplan];
    [self salvarMudancas];
}

-(void)carregarExercisePlan {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_ExercisePlan"];
        [requisicao setEntity:entidade];
        
        //NSPredicate *predicate = [NSPredicate predicateWithFormat: @"ORDER BY grupo"];
        //[requisicao setPredicate:predicate];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"data_begin" ascending:NO];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta ExercisePlan" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarExercisePlan];
    return sucess;
}

-(S4M_ExercisePlan *)criarExercisePlan:(NSString *)name setDateBegin:(NSDate *)datebegin setDateEnd:(NSDate *)dateend setHour:(NSString *)hour setObjective:(NSString *)objective{
    S4M_ExercisePlan *exercise = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_ExercisePlan" inManagedObjectContext:contexto];
    [exercise setName:name];
    [exercise setDate_begin:datebegin];
    [exercise setDate_end:dateend];
    [exercise setHour:hour];
    [exercise setObjective:objective];
    [itens addObject:exercise];
    
    [self salvarMudancas];
    [self carregarExercisePlan];
    return exercise;
}

-(S4M_ExercisePlan *)upgradeExercisePlan:(int) pos setName:(NSString *)name setDateBegin:(NSDate *)datebegin setDateEnd:(NSDate *)dateend setHour:(NSString *)hour setObjective:(NSString *)objective{
    [self carregarExercisePlan];
    S4M_ExercisePlan *exercise = [itens objectAtIndex:pos];
    [exercise setName:name];
    [exercise setDate_begin:datebegin];
    [exercise setDate_end:dateend];
    [exercise setHour:hour];
    [exercise setObjective:objective];
    [self salvarMudancas];
    [self carregarExercisePlan];
    return exercise;
}

-(void)excluirExercicioPlan:(S4M_ExercisePlan *)exerc {
    [contexto deleteObject:exerc];
    [itens removeObjectIdenticalTo:exerc];
}

@end
