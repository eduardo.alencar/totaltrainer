//
//  Trainer.m
//  TotalTrainer
//
//  Created by Eduardo on 29/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import "Trainer.h"

@implementation Trainer

@synthesize exercicio;
@synthesize grupo;
@synthesize repeticao;
@synthesize carga;
@synthesize tid;

-(id)init {
    self = [super init];
    if (self) {
        exercicio = [[NSString alloc] init];
        carga = [[NSString alloc] init];
        grupo = [[NSString alloc] init];
        repeticao = [[NSString alloc] init];
        tid = [[NSString alloc] init];
    }
    return self;
}

@end
