//
//  Global.h
//  TotalTrainer
//
//  Created by Eduardo on 29/06/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sql.h"
#import "BodyFat.h"
#import "S4M_Exercise.h"
#import <FacebookSDK/FacebookSDK.h>

extern NSString *user, *fbID, *user_id, *treino_id, *VERSION;
extern Boolean fbB, SOUND, SPEAK, DEBUG_LOG;
extern NSMutableArray *aTrainer;
extern int treinogeral;
extern S4M_Exercise *exerc_detalhe;
extern int contG, maxG;
extern UITextField *textAlertGroup, *textAlertExercise, *textAlertSerie, *textAlertRepeat,*textAlertweight,  *tbfAlertweight, *tbfAlertCoxa, *tbfAlertPeito, *tbfAlertAbdomen, *tbfAlertIdade, *tbfAlertPeso;
extern NSURL *ubiq;
extern UIView *viewFront;

@interface Global : NSObject {
    
}

@end
