//
//  S4M_Setup.h
//  TotalTrainer
//
//  Created by Eduardo on 29/08/13.
//  Copyright (c) 2013 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_Setup : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * facebook;
@property (nonatomic, retain) NSString * runkeeper;
@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSString * pass;
@property (nonatomic, retain) NSNumber * gps;
@property (nonatomic, retain) NSNumber * total_hour;
@property (nonatomic, retain) NSNumber * total_min;
@property (nonatomic, retain) NSNumber * interval_min;
@property (nonatomic, retain) NSNumber * interval_sec;
@property (nonatomic, retain) NSString * unit;
@property (nonatomic, retain) NSString * grupo;
@property (nonatomic, retain) NSNumber * sound;
@property (nonatomic, retain) NSNumber * speak;

@end
