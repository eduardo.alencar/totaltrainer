//
//  S4M_PhysicalDobra.m
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "S4M_PhysicalDobra.h"


@implementation S4M_PhysicalDobra

@dynamic abdominal;
@dynamic id;
@dynamic panturrilha;
@dynamic peitoral;
@dynamic physicalplan_id;
@dynamic quadriceps;
@dynamic subescapular;
@dynamic suprailiaca;
@dynamic triciptal;

@end
