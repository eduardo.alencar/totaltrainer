//
//  SetupViewController.h
//  myTimer
//
//  Created by Eduardo on 23/09/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Setup.h"
#import "Global.h"
#import <FacebookSDK/FacebookSDK.h>

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface SetupViewController : UIViewController<UITextFieldDelegate>{
    IBOutlet UITextField *total_hour, *total_min, *interval_min, *interval_sec;
    IBOutlet UILabel *ltitle, *ltrainer, *linterval, *lfacebook, *lsound, *lspeak, *lhelp1;
    IBOutlet UIButton *saveSetup, *s4m_page, *sfacebook, *ssound, *sspeak;
    IBOutlet UIAlertView *alert;
    NSMutableArray *linfo;
    NSNumber *um, *zero;
}

@property(nonatomic, retain) IBOutlet UITextField *total_hour, *total_min, *interval_min, *interval_sec;
@property(nonatomic, retain) IBOutlet UIButton *saveSetup, *s4m_page, *sfacebook, *ssound, *sspeak;
@property(nonatomic, retain) IBOutlet UILabel *ltitle, *ltrainer, *linterval, *lfacebook, *lsound, *lspeak, *lhelp1;

-(void)reloadInfo;
-(IBAction)sendSaveSetup:(id) sender;
-(IBAction)sendS4M:(id) sender;
-(IBAction)sendBack:(id) sender;
-(IBAction)buttonFace:(id)sender;
-(IBAction)buttonSound:(id)sender;
-(IBAction)buttonSpeak:(id)sender;
-(IBAction)getTotalHour:(id)sender;
-(IBAction)getTotalInterval:(id)sender;

@end
