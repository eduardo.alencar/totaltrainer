//
//  SetupViewController.m
//  myTimer
//
//  Created by Eduardo on 23/09/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import "SetupViewController.h"

@interface SetupViewController ()

@end

@implementation SetupViewController

@synthesize total_hour;
@synthesize total_min;
@synthesize interval_min;
@synthesize interval_sec;
@synthesize saveSetup, s4m_page, sfacebook, ssound, sspeak;
@synthesize lfacebook, linterval, ltitle, ltrainer, lsound, lspeak, lhelp1;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Loading",@"") delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alert show];
    linfo = [[NSMutableArray alloc] init];
    
    um = [NSNumber numberWithInt:1];
    zero = [NSNumber numberWithInt:0];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    if (IS_IPHONE_5) [[UIImage imageNamed:@"fundo-massa-corporal-02.png"] drawInRect:self.view.bounds];
    else [[UIImage imageNamed:@"fundo-massa-corporal-01.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
     
    [sfacebook setImage:[UIImage imageNamed:@"bot-selec-on-01.png"] forState:UIControlStateSelected];
    [sfacebook setImage:[UIImage imageNamed:@"bot-selec-off-01.png"] forState:UIControlStateNormal];
    [ssound setImage:[UIImage imageNamed:@"bot-selec-on-01.png"] forState:UIControlStateSelected];
    [ssound setImage:[UIImage imageNamed:@"bot-selec-off-01.png"] forState:UIControlStateNormal];
    [sspeak setImage:[UIImage imageNamed:@"bot-selec-on-01.png"] forState:UIControlStateSelected];
    [sspeak setImage:[UIImage imageNamed:@"bot-selec-off-01.png"] forState:UIControlStateNormal];

    [saveSetup setFont:[UIFont fontWithName:@"Exo-Medium" size:16]];
    
    [ltitle setFont:[UIFont fontWithName:@"DINEngschriftStd" size:24]];
    //[ltrainer setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Bd" size:20]];
    //[linterval setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Bd" size:20]];
    //[lfacebook setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Bd" size:20]];
    
    //[total_hour setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Md" size:18]];
    //[total_min setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Md" size:18]];
    //[interval_min setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Md" size:18]];
    //[interval_sec setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Md" size:18]];
    
    [ltitle setText:NSLocalizedString(@"set_title", @"")];
    [lsound setText:NSLocalizedString(@"set_sound", @"")];
    [lspeak setText:NSLocalizedString(@"set_speaker", @"")];
    [lhelp1 setText:NSLocalizedString(@"set_help1", @"")];
    [ltrainer setText:NSLocalizedString(@"set_trainer", @"")];
    [linterval setText:NSLocalizedString(@"set_interval", @"")];
    [saveSetup setTitle:NSLocalizedString(@"set_save", @"") forState:UIControlStateNormal];
    
    [total_hour setDelegate:self];
    [total_min setDelegate:self];
    [interval_min setDelegate:self];
    [interval_sec setDelegate:self];
    
    [self reloadInfo];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    if (DEBUG_LOG) NSLog(@"Lendo ShouldReturn ...");
    if (theTextField == total_hour) {
        if (DEBUG_LOG) NSLog(@"Lendo total_HOUR ...");
        [total_min becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-100, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else if (theTextField == total_min) {
        if (DEBUG_LOG) NSLog(@"Lendo total_MIN ...");
        [interval_min becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-140, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else if (theTextField == interval_min) {
        if (DEBUG_LOG) NSLog(@"Lendo interval_MIN ...");
        [interval_sec becomeFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-140, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else if (theTextField == interval_sec) {
        if (DEBUG_LOG) NSLog(@"Lendo interval_SEC ...");
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,-0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    } else {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.30];
        self.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
    return YES;
}


-(void)reloadInfo {
    // MIGRAR PARA COREDATA
    ///linfo = [banco getTimes];
    NSLog(@"INCIANDO COLETA iCLOUD");
    //[[Setup setupCompartilhado] carregarSetup];
    linfo = [[Setup setupCompartilhado] itens];
    //linfo = [[[Setup setupCompartilhado] itens] objectAtIndex:0];
    NSLog(@"COLETA iCLOUD %@", linfo);
    
    if (DEBUG_LOG) NSLog(@"LISTA DE SETUP -> %@", linfo );
    if ([linfo count] > 0) {
        S4M_Setup *setup = [linfo objectAtIndex:0];
        if (DEBUG_LOG) NSLog(@"ENTROU NO LUGAR DA LEITURA DOS VALORES ... %@ ", linfo);
        [sfacebook setSelected:[setup facebook] == [NSNumber numberWithInt:0] ? FALSE : TRUE];
        [total_hour setText:[[setup total_hour] stringValue]];
        [total_min  setText:[[setup total_min] stringValue]];
        [interval_min setText:[[setup interval_min] stringValue]];
        [interval_sec setText:[[setup interval_sec] stringValue]];
        [ssound setSelected:[setup sound] == [NSNumber numberWithInt:0] ? FALSE : TRUE];
        [sspeak setSelected:[setup speak] == [NSNumber numberWithInt:0] ? FALSE : TRUE];
    } else {
        [sfacebook setSelected:YES];
        [total_hour setText:@"00"];
        [total_min  setText:@"45"];
        [interval_min setText:@"00"];
        [interval_sec setText:@"30"];
        [ssound setSelected:YES];
        [sspeak setSelected:NO];
    }
}

-(IBAction)sendSaveSetup:(id) sender{
    [interval_min resignFirstResponder];
    [interval_sec resignFirstResponder];
    [total_hour resignFirstResponder];
    [total_min resignFirstResponder];
    
    NSString *th = [total_hour text];
    NSString *tm = [total_min text];
    NSString *im = [interval_min text];
    NSString *is = [interval_sec text];
    int ith, itm, iim, iis;
    if (DEBUG_LOG) NSLog(@"PASSO 01");
    BOOL check = YES;
    
    if ([th length] == 0) [total_hour setText:@"00"];
    if ([tm length] == 0) [total_min setText:@"45"];
    if ([im length] == 0) [interval_min setText:@"00"];
    if ([is length] == 0) [interval_sec setText:@"45"];
    
    if ([[NSScanner scannerWithString:th] scanInt:nil]) ith = [th intValue];
    else {
        check = NO;
        [total_hour setText:@"00"];
    }
    if ([[NSScanner scannerWithString:tm] scanInt:nil]) itm = [tm intValue];
    else {
        check = NO;
        [total_min setText:@"00"];
    }

    if ([[NSScanner scannerWithString:im] scanInt:nil]) iim = [im intValue];
    else {
        check = NO;
        [interval_min setText:@"00"];
    }
    if ([[NSScanner scannerWithString:im] scanInt:nil]) iis = [is intValue];
    else {
        check = NO;
        [interval_sec setText:@"00"];
    }
    if (DEBUG_LOG) NSLog(@"PASSO 02");
    if (check) {
        /*
        NSMutableArray *lista = [[NSMutableArray alloc] init];
        [lista addObject: [sfacebook isSelected] ? @"YES" : @"NO"]; // facebook
        if (!sfacebook.selected) fbID = @"";
        [lista addObject: fbID]; // login
        [lista addObject: @""]; // ps
        [lista addObject: @"NO"]; // gps
        [lista addObject:[NSString stringWithFormat:@"%02i",ith]]; // total_hour
        [lista addObject:[NSString stringWithFormat:@"%02i",itm]]; // total_min
        [lista addObject:[NSString stringWithFormat:@"%02i",iim]]; // interval_min
        [lista addObject:[NSString stringWithFormat:@"%02i",iis]]; // interval_sec
        [lista addObject:@"A"];// grupo
        [lista addObject: [ssound isSelected] ? @"YES" : @"NO"]; // sound
        [lista addObject: [sspeak isSelected] ? @"YES" : @"NO"]; // speak
        
        //MIGRAR PARA COREDATA
        //[banco atualizarTimes:lista];
        */
        if (DEBUG_LOG) NSLog(@"PASSO 03");
        [[Setup setupCompartilhado] upgradeSetup:@"" setLogin:fbID setFacebook:[sfacebook isSelected] ? um : zero setGPS:zero setTotalHour:[NSNumber numberWithInt:[th intValue]] setTotalMin:[NSNumber numberWithInt:[tm intValue]] setIntervalMin:[NSNumber numberWithChar:[im intValue]] setIntervalSec:[NSNumber numberWithChar:[is intValue]] setUnit:@"" setGrupo:@"A" setSound:[ssound isSelected]? um : zero setSpeak:[sspeak isSelected] ? um : zero];
         
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Success", @"") delegate:nil cancelButtonTitle:@"OK"otherButtonTitles: nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TotalTrainer" message:NSLocalizedString(@"Numbers!",@"") delegate:nil cancelButtonTitle:@"OK"otherButtonTitles: nil];
        [alert show];
    }
}

-(IBAction)sendBack:(id) sender{
   //[self.view removeFromSuperview];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)buttonFace:(id)sender {
    sfacebook.selected = !sfacebook.selected;
    fbB = [sfacebook isSelected];
}

-(IBAction)buttonSound:(id)sender {
    ssound.selected = !ssound.selected;
    SOUND = [ssound isSelected];
}

-(IBAction)buttonSpeak:(id)sender {
    sspeak.selected = !sspeak.selected;
    SPEAK = [sspeak isSelected];
}

-(IBAction)getTotalHour:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    self.view.frame = CGRectMake(0,-100, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}

-(IBAction)getTotalInterval:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.30];
    self.view.frame = CGRectMake(0,-140, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
}

@end
