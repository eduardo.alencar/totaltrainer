//
//  S4M_PhysicalPerime.m
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "S4M_PhysicalPerime.h"


@implementation S4M_PhysicalPerime

@dynamic abdomen;
@dynamic antebraco;
@dynamic biceps;
@dynamic cintura;
@dynamic coxa;
@dynamic id;
@dynamic panturrilha;
@dynamic peitoral;
@dynamic physicalplan_id;
@dynamic quadril;

@end
