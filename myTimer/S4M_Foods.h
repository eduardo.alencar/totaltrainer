//
//  S4M_Foods.h
//  TotalTrainer
//
//  Created by Eduardo on 03/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface S4M_Foods : NSManagedObject

@property (nonatomic, retain) NSString * calories;
@property (nonatomic, retain) NSString * carbohydrates;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * proteins;
@property (nonatomic, retain) NSString * sodio;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * category;

@end
