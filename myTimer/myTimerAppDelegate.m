//
//  myTimerAppDelegate.m
//  myTimer
//
//  Created by Eduardo on 20/08/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import "myTimerAppDelegate.h"
#import <FacebookSDK/FacebookSDK.h> 

@implementation myTimerAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    DEBUG_LOG = YES;
    VERSION = @"3.0";
   
    UIStoryboard *mainStoryboard = nil;
    if (IS_IPHONE_5) mainStoryboard = [UIStoryboard storyboardWithName:@"Storyboard_iphone5" bundle:nil];
    else mainStoryboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [mainStoryboard instantiateInitialViewController];
    [self.window makeKeyAndVisible];

    ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    if (ubiq) {
        if (DEBUG_LOG) NSLog(@" ****************** iCloud access at %@", ubiq);
        // TODO: Load document...
    } else {
        if (DEBUG_LOG) NSLog(@"*******************  No iCloud access");
        // colocar alert avisando da falha do iCloud
    }

    //NavigationBar
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 0);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:10.0/255.0 green:10.0/255.0 blue:10.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           shadow, NSShadowAttributeName,
                                                           [UIFont fontWithName:@"HelveticaNeueLTStd-Md" size:21.0], NSFontAttributeName, nil]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [FBSession.activeSession close];

}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication fallbackHandler:^(FBAppCall *call) { if (DEBUG_LOG)  NSLog(@"In fallback handler"); }];
    return TRUE;
}

@end
