//
//  myTimerViewController.h
//  myTimer
//
//  Created by Eduardo on 20/08/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAD.h>
//#import "LocationController.h"
#import "SetupViewController.h"
#import "TrainerTableViewController.h"
#import "BodyFatViewController.h"
#import "LoginViewController.h"
#import "Sql.h"
#import "MachTimer.h"
#import "Global.h"
#import <Slt/Slt.h>
//#import <Slt8k/Slt8k.h>
#import <OpenEars/FliteController.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <QuartzCore/QuartzCore.h>
#import "TableMenuViewController.h"

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define POLARH7_HRM_DEVICE_INFO_SERVICE_UUID @"180A"
#define POLARH7_HRM_HEART_RATE_SERVICE_UUID @"180D"
#define POLARH7_HRM_MEASUREMENT_CHARACTERISTIC_UUID @"2A37"
#define POLARH7_HRM_BODY_LOCATION_CHARACTERISTIC_UUID @"2A38"
#define POLARH7_HRM_MANUFACTURER_NAME_CHARACTERISTIC_UUID @"2A29"

@protocol myTimerViewControllerDelegate <NSObject>
@optional
- (void)movePanelLeft;

@required
- (void)movePanelToOriginalPosition;

@end

@interface myTimerViewController : UIViewController <ADBannerViewDelegate, CLLocationManagerDelegate, CBCentralManagerDelegate, CBPeripheralDelegate, TableMenuViewControllerDelegate> {
    
    SetupViewController *setupController;
    TrainerTableViewController *trainerController;
    BodyFatViewController *bfatController;
    
    FliteController *fliteController;
    Slt *slt;
    UILabel *linterval, *ltotal, *lexercicio, *lcarga, *lgrupo, *lrepeticao, *ltitle_interval, *ltitle_total;
    UIButton *blogout, *bavalicao, *btrainer, *bcontinue, *bUp, *bDown, *bSetup;
    UIAlertView *alert;
    UIImageView *imageGroup;
    UIColor *colorGray, *colorRed;
    BOOL stop, finalTreino;
    int countDown, min, hora, segs;
    int tmin, thora, tsegs, totsegs, posTreino;
    NSTimer *countDownTimer, *countTotal;
    NSMutableArray *list, *listTrainer;
    NSArray *aalf;
    NSURL *kLatestKivaLoansURL;
    CLGeocoder *geocoder;
    
    AVAudioPlayer *audioPlayer;
    ADBannerView *adBannerView;
    Trainer *treino;
}

@property (nonatomic, assign) id<myTimerViewControllerDelegate> delegate;
@property(nonatomic, retain) IBOutlet UILabel *linterval, *ltotal, *lexercicio, *lcarga, *lgrupo, *lrepeticao, *ltitle_interval, *ltitle_total;
@property(nonatomic, retain) IBOutlet UIView *contentView;
@property(nonatomic, retain) IBOutlet UIImageView *imageGroup;
@property(nonatomic, retain) IBOutlet UIButton *blogout, *bavalicao, *btrainer, *bcontinue, *bUp, *bDown, *bSetup;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property(nonatomic, retain) ADBannerView *adBannerView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, retain) SetupViewController *setupController;
@property(nonatomic, retain) TrainerTableViewController *trainerController;
@property(nonatomic, retain) BodyFatViewController *bfatController;
@property (strong, nonatomic) NSMutableArray *notes, *list;
@property (strong, nonatomic) id<FBGraphUser> loggedInUser;
@property (nonatomic, strong) NSDate *beginTime;
@property(strong, nonatomic) FliteController *fliteController;
@property(strong, nonatomic) Slt *slt;
@property(nonatomic, strong) CBCentralManager *centralManager;
@property(nonatomic, strong) CBPeripheral *polarH7HRMPeripheral;

-(IBAction)clickedConfig:(id)sender;
-(IBAction)clickedContinue:(id)sender;
-(void) startCountDown:(int)sec;
-(void)updateTime:(NSTimer *)timerParam;
-(void) clearCountDownTimer;
-(void)formatTime;
-(void)updateTrainer;
-(void)updateListTrainer:(NSString *)Day;
-(IBAction)logoutClick:(UIButton *)sender;
-(void)updateFaceBook2;
-(IBAction)TrainerClick:(UIButton *)sender;
-(IBAction)BFClick:(UIButton *)sender;
-(IBAction)BUpClick:(UIButton *)sender;
-(IBAction)BDownClick:(UIButton *)sender;
@end
