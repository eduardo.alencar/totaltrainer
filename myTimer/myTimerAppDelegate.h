//
//  myTimerAppDelegate.h
//  myTimer
//
//  Created by Eduardo on 20/08/12.
//  Copyright (c) 2012 Solution4Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface myTimerAppDelegate : UIResponder <UIApplicationDelegate> {
    
}

@property (strong, nonatomic) UIWindow *window;

@end
