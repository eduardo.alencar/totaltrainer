//
//  PhysicalPlan.m
//  TotalTrainer
//
//  Created by Eduardo on 08/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "PhysicalPlan.h"

@implementation PhysicalPlan

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSError *erro = nil;
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarPhysicalPlan];
    }
    return self;
}

+(PhysicalPlan *)physicalPlanCompartilhado {
    static PhysicalPlan *physicalPlanCompartilhado = nil;
    if (!physicalPlanCompartilhado) physicalPlanCompartilhado = [[super allocWithZone:nil] init];
    return physicalPlanCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self physicalPlanCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_PhysicalPlan *)physicalplan {
    [itens addObject:physicalplan];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_PhysicalPlan *)physicalplan {
    [contexto deleteObject:physicalplan];
    [itens removeObject:physicalplan];
    [self salvarMudancas];
}

-(void)carregarPhysicalPlan {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_PhysicalPlan"];
        [requisicao setEntity:entidade];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"data" ascending:NO];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta PhysicalPlan" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarPhysicalPlan];
    return sucess;
}

-(S4M_PhysicalPlan *)criarPhysicalPlan:(NSString *)date setHeight:(NSNumber *)cm setBF:(NSNumber *)BF setWeight:(NSNumber *)kg setObj:(NSString *)info {
    S4M_PhysicalPlan *physical = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_PhysicalPlan" inManagedObjectContext:contexto];
    date = [NSString stringWithFormat:@"%@%@%@",[date substringWithRange:NSMakeRange(6,4)],[date substringWithRange:NSMakeRange(3,2)],[date substringWithRange:NSMakeRange(0,2)]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSDate *date2 = [dateFormat dateFromString:date];
    [physical setData:date2];
    [physical setAltura:cm];
    [physical setBf:BF];
    [physical setPeso:kg];
    [physical setObjetivo:info];
    [itens addObject:physical];
    [self salvarMudancas];
    [self carregarPhysicalPlan];
    return physical;
}

-(S4M_PhysicalPlan *)upgradePhysicalPlan:(int)pos setDate:(NSString *)date setHeight:(NSNumber *)cm setBF:(NSNumber *)BF setWeight:(NSNumber *)kg setObj:(NSString *)info {
    [self carregarPhysicalPlan];
    S4M_PhysicalPlan *physical = [itens objectAtIndex:pos];
    date = [NSString stringWithFormat:@"%@%@%@",[date substringWithRange:NSMakeRange(6,4)],[date substringWithRange:NSMakeRange(3,2)],[date substringWithRange:NSMakeRange(0,2)]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSDate *date2 = [dateFormat dateFromString:date];
    [physical setData:date2];
    [physical setAltura:cm];
    [physical setBf:BF];
    [physical setPeso:kg];
    [physical setObjetivo:info];
    [self salvarMudancas];
    [self carregarPhysicalPlan];
    return physical;
}

-(void)excluirPhysicalPlan:(S4M_PhysicalPlan *)physicalplan {
    [contexto deleteObject:physicalplan];
    [itens removeObjectIdenticalTo:physicalplan];
}


@end
