//
//  PhysicalPerime.m
//  TotalTrainer
//
//  Created by Eduardo on 08/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "PhysicalPerime.h"

@implementation PhysicalPerime

-(id)init{
    self = [super init];
    if (self) {
        modelo = [NSManagedObjectModel mergedModelFromBundles:nil];
        psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:modelo];
        urlArmazenamento = [ubiq URLByAppendingPathComponent:@"s4m_totaltrainer.sqlite"];
        NSError *erro = nil;
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,[NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:urlArmazenamento options:options error:&erro];
        contexto = [[NSManagedObjectContext alloc] init];
        [contexto setPersistentStoreCoordinator:psc];
        [self carregarPhysicalperime];
    }
    return self;
}

+(PhysicalPerime *)physicalPerimeCompartilhado {
    static PhysicalPerime *physicalPerimeCompartilhado = nil;
    if (!physicalPerimeCompartilhado) physicalPerimeCompartilhado = [[super allocWithZone:nil] init];
    return physicalPerimeCompartilhado;
}

+(id)allocWithZone:(NSZone *)zone {
    return [self physicalPerimeCompartilhado];
}

-(NSArray *)itens {
    return itens;
}

-(void)adicionarItem:(S4M_PhysicalPerime *)physicalPerime{
    [itens addObject:physicalPerime];
    [itens sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self salvarMudancas];
}

-(void)removerItem:(S4M_PhysicalPerime *)physicalPerime {
    [contexto deleteObject:physicalPerime];
    [itens removeObject:physicalPerime];
    [self salvarMudancas];
}

-(void)carregarPhysicalperime {
    if (!itens) {
        NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
        NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_PhysicalPerime"];
        [requisicao setEntity:entidade];
        
        //NSPredicate *predicate = [NSPredicate predicateWithFormat: @"ORDER BY grupo"];
        //[requisicao setPredicate:predicate];
        NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"physicalplan_id" ascending:NO];
        [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
        NSError *erro;
        NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
        if (!resultado) [NSException raise:@"Falha na consulta PhysicalPerime" format:@"Motivo: %@", [erro localizedDescription]];
        itens = [[NSMutableArray alloc] initWithArray:resultado];
    }
}

-(NSMutableArray *)carregarPhysicalperime:(NSString *)plan_id {
    NSFetchRequest *requisicao = [[NSFetchRequest alloc] init];
    NSEntityDescription *entidade = [[modelo entitiesByName] objectForKey:@"S4M_PhysicalPerime"];
    [requisicao setEntity:entidade];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"plan_id CONTAINS %@ ",plan_id];
    [requisicao setPredicate:predicate];
    NSSortDescriptor *descritorDeOrdenacao = [NSSortDescriptor sortDescriptorWithKey:@"physicalplan_id" ascending:YES];
    [requisicao setSortDescriptors:@[descritorDeOrdenacao]];
    NSError *erro;
    NSArray *resultado = [contexto executeFetchRequest:requisicao error:&erro];
    if (!resultado) [NSException raise:@"Falha na consulta PhysicalPerime" format:@"Motivo: %@", [erro localizedDescription]];
    //NSMutableArray *listtotal =  [[NSMutableArray alloc] initWithArray:resultado];
    NSMutableArray *listtotal =  [resultado copy];
    NSMutableArray *listaG = [[NSMutableArray alloc] init];
    for (int i = 0; [listtotal count] > i; i++) {
        
    }
    return listaG;
}

-(BOOL)salvarMudancas{
    NSError *erro;
    BOOL sucess = [contexto save:&erro];
    if (!sucess) NSLog(@"Falha ao salvar os itens : %@", [erro localizedDescription]);
    itens = nil;
    [self carregarPhysicalperime];
    return sucess;
}

-(S4M_PhysicalPerime *)criarPhysicalperime:(NSString *)physicalplan_id setAbdomen:(NSNumber *)abdomen setAntebraco:(NSNumber *)antebraco setBiceps:(NSNumber *)biceps  setCintura:(NSNumber *)cintura setCoxa:(NSNumber *)coxa setPanturrilha:(NSNumber *)panturrilha setPeitoral:(NSNumber *)peitoral setQuadril:(NSNumber *)quadril {
    S4M_PhysicalPerime *physical = [NSEntityDescription insertNewObjectForEntityForName:@"S4M_PhysicalPerime" inManagedObjectContext:contexto];
    [physical setPhysicalplan_id:[NSNumber numberWithInteger: [physicalplan_id integerValue]]];
    [physical setAbdomen:abdomen];
    [physical setAntebraco:antebraco];
    [physical setBiceps:biceps];
    [physical setCintura:cintura];
    [physical setCoxa:coxa];
    [physical setPanturrilha:panturrilha];
    [physical setPeitoral:peitoral];
    [physical setQuadril:quadril];
    [itens addObject:physical];
    
    [self salvarMudancas];
    [self carregarPhysicalperime];
    return physical;
}

-(S4M_PhysicalPerime *)upgradePhysicalperime:(int)pos setPlan:(NSString *)physicalplan_id setAbdomen:(NSNumber *)abdomen setAntebraco:(NSNumber *)antebraco setBiceps:(NSNumber *)biceps  setCintura:(NSNumber *)cintura setCoxa:(NSNumber *)coxa setPanturrilha:(NSNumber *)panturrilha setPeitoral:(NSNumber *)peitoral setQuadril:(NSNumber *)quadril {
    [self carregarPhysicalperime];
    S4M_PhysicalPerime *physical = [itens objectAtIndex:pos];
    [physical setPhysicalplan_id:[NSNumber numberWithInteger: [physicalplan_id integerValue]]];
    [physical setAbdomen:abdomen];
    [physical setAntebraco:antebraco];
    [physical setBiceps:biceps];
    [physical setCintura:cintura];
    [physical setCoxa:coxa];
    [physical setPanturrilha:panturrilha];
    [physical setPeitoral:peitoral];
    [physical setQuadril:quadril];
    [self salvarMudancas];
    [self carregarPhysicalperime];
    return physical;
}

-(void)excluirPhysicalperime:(S4M_PhysicalPerime *)physicalperime {
    [contexto deleteObject:physicalperime];
    [itens removeObjectIdenticalTo:physicalperime];
}

@end
