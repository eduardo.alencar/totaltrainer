//
//  EditWorkoutViewController.m
//  TotalTrainer
//
//  Created by Eduardo on 10/02/14.
//  Copyright (c) 2014 Solution4Mac. All rights reserved.
//

#import "EditWorkoutViewController.h"

@interface EditWorkoutViewController ()

@end

@implementation EditWorkoutViewController

@synthesize bsave;
@synthesize bworkout;
@synthesize tdatabegin;
@synthesize ldatabegin;
@synthesize ldataend;
@synthesize tdataend;
@synthesize ltitle;
@synthesize title;
@synthesize thour;
@synthesize lhour;
@synthesize tobj;
@synthesize lobj;
@synthesize lname;
@synthesize tname;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)savedWorkoutPlan:(id)sender {
    
}

-(IBAction)editWorkoutPlan:(id)sender {
    
}

@end
